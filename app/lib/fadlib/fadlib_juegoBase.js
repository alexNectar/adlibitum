FAdLib_Game = function(_mainDiv, _params, _gameInterface) {
    console.log(":: FAdlib : Juego <Template> ::");   
    
    var running=false;
    
    var mainDiv=_mainDiv;
    var params=_params;
    var gameInterface=_gameInterface;
    
    //parametros basicos (requeridos) : valor por defecto
    var paramsDefault={
        pentagrama: true,
        notas: true,
        velocidad: 100,
        previsualizar: 2000,
        soloTrack: null
    }
    for (var i in paramsDefault) {
        if (params[i]==undefined) params[i]=paramsDefault[i];
    }
    
    // -----------------------------------------------------------------------------
    
    var htmlLayout=`
    <div class="fadlib full vertical" id="fondo">
        <div class="fadlib horizontal full" id="cabecera">
            <div class="fadlib" id="controles">
                <img class="fadlib start" id="startstop" />
            </div>
            <div class="fadlib" id="marcadores">
                <div class="fadlib right" id="marcador">Marcadores</div>
            </div>
        </div>
        <div class="fadlib canvas" id="juego">
            <img class="fadlib capa pentagrama" />
            <img class="fadlib capa vcenter clave" />
            <div class="fadlib capa vcenter compasText" id="compasText" />
            <img class="fadlib capa cabezalectora hcenter" />
            <div class="fadlib capa full" id="notas"></div>
        </div>
        <div class="fadlib center vcenter debug" id="lanzadores">Lanzadores</div>
    </div>
    `;

    //      fadlib capa vcenter 
    // -----------------------------------------------------------------------------
    
    function isRunning() {
        return running;
    }
    
    var elJuego=null;
    var elNotas=null;
    var elMarcador=null;
    var elStartStop=null;
    function init() {
        // validar parametros (valores minimos)
        if (params.velocidad<1) params.velocidad=1;
        if (params.previsualizar<1) params.previsualizar=1;
        
        // crear elementos visuales
        
        mainDiv.append(htmlLayout);
        
        //elJuego = $(".fadlib#juego");
        elNotas = $(".fadlib#notas");
        elMarcador = $(".fadlib#marcador");
        
        if (params.pentagrama==false) $(".fadlib.pentagrama").hide();
        if (params.notas==false || params.pentagrama==false) $(".fadlib.clave").hide();
        
        elStartStop=$(".fadlib#startstop").click(function(e){
            if (running) stop(); else start();
        });
        
        updateMarcador();        
    }
    
    
    var tiempoInicio=null;
    var tiempoJuego=0;
    var juegoCompletado=false;
    
    function start() {
        if (running) return;
        
        tiempoInicio=new Date().getTime();  // miliseconds clock
        tiempoJuego=0;
        juegoCompletado=false;
        
        running=true;
        // informar de que el juego se ha iniciado -por usuario o auto-
        if (startCallback && typeof startCallback==="function") startCallback.call();
    }
    
    
    function stop() {
        if (!running) return;
        
        juegoCompletado=false;
        
        running=false;
        // informar de que el usuario lo ha detenido -manualmente-
        if (stopCallback && typeof stopCallback==="function") stopCallback.call();
    }
    
    
    function updateMarcador() {
        // mostrar marcador, progreso realizado
        //var p=gameInterface.midiPlayer.getProgress(true);
        //var t=gameInterface.midiPlayer.getTotalLength();
        //elMarcador.text(p+"/"+t+"   ("+Math.round(100*p/t)+"%)");
        
        // informar del progreso
        if (progressCallback && typeof progressCallback==="function") progressCallback.call(null);
    }
    
    function end() {
        // midi finalizado
        // ---auto stop---
        //detenerJuego();
        //elStartStop.removeClass("stop").addClass("start");
        juegoCompletado=true;
        
        // fin del juego : informar
        if (endCallback && typeof endCallback==="function") endCallback.call();
        
        tiempoInicio=null;
    }
    
    // ---------------------- game Data
    function getData() {
        if (tiempoInicio!=null) tiempoJuego=((new Date().getTime())-tiempoInicio)/1000; // tiempo en secs.
        
        return {
            aciertos: 0,
            errores: 0,
            puntos: 0,
            tiempo: tiempoJuego,
            completado: juegoCompletado
        }
    }
    
    // ------------------- set Events Callback ----------------------------------
    var startCallback=null;
    function setStartCallback(callback) {
        startCallback=callback;
    }
    var endCallback=null;
    function setEndCallback(callback) {
        endCallback=callback;
    }
    var stopCallback=null;
    function setStopCallback(callback) {
        stopCallback=callback;
    }
    
    var progressCallback=null;
    function setProgressCallback(callback) {
        progressCallback=callback;
    }
    
    
    // ------------------- public -----------------------------------------------
    
    this.init=init;
    this.start=start;
    this.stop=stop;
    this.isRunning=isRunning;
    this.getData=getData;
    this.setStartCallback=setStartCallback;
    this.setEndCallback=setEndCallback;
    this.setStopCallback=setStopCallback;
    this.setProgressCallback=setProgressCallback;

    return this;
}
    