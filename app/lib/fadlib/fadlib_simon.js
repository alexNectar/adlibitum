FAdLib_Game = function(_mainDiv, _params, _gameInterface) {
    console.log(":: FAdlib : Juego Simon ::");   
    
    var running=false;
    
    var mainDiv=_mainDiv;
    var params=_params;
    var gameInterface=_gameInterface;
    
    //parametros basicos (requeridos) : valor por defecto
    var paramsDefault={
        pentagrama: true,
        notas: true,
        velocidad: 100,
        previsualizar: 2000,
        soloTrack: null,
        puntos: {
            nota: 10,
            secuencia: 100,
            fallo: -10
        },
        secuencias: {
            tipo: "midi",       // "random", "random-sec", "midi", midi-sec"
            inicio: 3,
            crecer: 2,
            ganar: 3
        },
        error: {
            nota: "a3",
            instrumento: 0
        }
    };
    for (var i in paramsDefault) {
        if (params[i]==undefined) params[i]=paramsDefault[i];
    }
    
    // -----------------------------------------------------------------------------
    
    var htmlLayout=`
    <div class="fadlib full vertical" id="fondo">
        <div class="fadlib horizontal full" id="cabecera">
            <div class="fadlib" id="controles">
                <img class="fadlib start" id="startstop" />
            </div>
            <div class="fadlib center" id="metronomo"></div>
            <div class="fadlib" id="marcadores">
                <div class="fadlib right" id="marcador">Marcadores</div>
            </div>
        </div>
        <div class="fadlib canvas" id="juego">
            <img class="fadlib capa pentagrama" />
            <img class="fadlib capa vcenter clave" />
            <div class="fadlib capa vcenter compasText" id="compasText" />
            <img class="fadlib capa cabezalectora hcenter" />
            <div class="fadlib capa full" id="notas"></div>
        </div>
        <div class="fadlib center vcenter" id="lanzadores"></div>
    </div>
    `;

    //      fadlib capa vcenter 
    // -----------------------------------------------------------------------------
    
    function isRunning() {
        return running;
    }
    
    var elJuego=null;
    var elNotas=null;
    var elMarcador=null;
    var elStartStop=null;
    var lanzaTeclas=[];
    function init() {
        // validar parametros (valores minimos)
        if (params.velocidad<1) params.velocidad=1;
        if (params.previsualizar<1) params.previsualizar=1;
        if (params.secuencias.tipo!="midi" &&
            params.secuencias.tipo!="midi-sec" &&
            params.secuencias.tipo!="random" &&
            params.secuencias.tipo!="random-sec") params.secuencias.tipo="midi";
        // crear elementos visuales
        
        mainDiv.append(htmlLayout);
        
        //elJuego = $(".fadlib#juego");
        elNotas = $(".fadlib#notas");
        elMarcador = $(".fadlib#marcador");
        
        if (params.pentagrama==false) $(".fadlib.pentagrama").hide();
        if (params.notas==false || params.pentagrama==false) $(".fadlib.clave").hide();
        
        // crear lanzadores
        if (params.lanzadores!=undefined && params.lanzadores instanceof Array) {
            lanzaTeclas=[];
            $(document).keydown(lanzadorTecla);
            var elLanzadores=$(".fadlib#lanzadores");
            for (var index in params.lanzadores) {
                var l=params.lanzadores[index];
                if (l!=null) {
                    var elLanza=$(document.createElement("img")).addClass("fadlib").addClass("lanzador");
                    if (l.class!=undefined) elLanza.addClass(l.class);
                    elLanzadores.append(elLanza);
                    var nota=l.nota || "a4";
                    var instrumento=l.instrumento || 0;
                    elLanza.data("lanzador", {nota:nota, instrumento:instrumento});
                    elLanza.click(lanzadorClick);
                    if (l.tecla!=undefined) {
                        lanzaTeclas.push({tecla:l.tecla, lanzador:elLanza});
                    }
                }
            }
        }
        
        if (params.secuencias.tipo!="random" && params.secuencias.tipo!="random-sec" && !gameInterface.midiPlayer.isReady()) gameInterface.showError("Simon: en modo 'midi' y 'midi-sec' necesito un archivo MIDI");
        
        // metronomo
        if (params.metronomo!=undefined) {
            gameInterface.midiPlayer.setMetronome(params.metronomo.instrumento, params.metronomo.nota, params.metronomo.step,params.metronomo.prestep);
            gameInterface.midiPlayer.setMetronomeCallback(metroCallback);
        } else $(".fadlib#metronomo").hide();
        
        // compas
        if (params.compas!=undefined) {
            var compas=params.compas.split("/");
            gameInterface.midiPlayer.setCompas(compas[0],compas[1]);
            gameInterface.midiPlayer.setCompasCallback(compasCallback);
            $(".fadlib#compasText").html(compas[0]+'</br>'+compas[1]);
        }
        
        // detectar silencios
        if (params.soloTrack!=undefined && params.canalSilencios!=undefined) {
            gameInterface.midiPlayer.detectarSilencios(params.soloTrack, params.canalSilencios);
        }
        
        elStartStop=$(".fadlib#startstop").click(function(e){
            if (running) stop(); else start();
        });
        
        updateMarcador();        
    }
    
    
    var tiempoInicio=null;
    var tiempoJuego=0;
    var juegoCompletado=false;
    var aciertos=0;
    var fallos=0;
    var puntos=0;
    var indexInicio=0;
    var lastIndex=0;
    var secuenciaLongitud=0;
    var secuenciaAciertos=0;
    var secuenciaNotas=[];
    var faseMostrando=true;
    
    function start() {
        if (running) return;
        
        tiempoInicio=new Date().getTime();  // miliseconds clock
        tiempoJuego=0;
        juegoCompletado=false;
        
        running=true;
        elStartStop.removeClass("start").addClass("stop");
        
        gameInterface.midiPlayer.setEndCallback(finMidi);
        
        tiempoInicio=new Date().getTime();  // miliseconds clock
        tiempoJuego=0;
        juegoCompletado=false;
        aciertos=0;
        fallos=0;
        puntos=0;
        indexInicio=0;
        lastIndex=0;
        secuenciaLongitud=params.secuencias.inicio;
        secuenciaAciertos=0;
        secuenciaNotas=[];
        faseMostrando=true;
        
        primeraSecuencia();
        
        updateMarcador();
        // informar de que el juego se ha iniciado -por usuario o auto-
        if (startCallback && typeof startCallback==="function") startCallback.call();
    }
    
    
    function stop() {
        if (!running) return;
        
        //detener el midi
        detenerJuego();
        elStartStop.removeClass("stop").addClass("start");
        juegoCompletado=false;
        
        // informar de que el usuario lo ha detenido -manualmente-
        if (stopCallback && typeof stopCallback==="function") stopCallback.call();
    }
    
    
    function updateMarcador() {
        // mostrar marcador, progreso realizado
        var marcador=
            "Long. Secuencia: "+secuenciaLongitud+
            `</br>`+"Sec.Completadas: "+secuenciaAciertos+
            `</br></br>`+"T.Aciertos = "+aciertos+
            `</br>`+"T.Fallos = "+fallos+
            `</br>`+"Puntos = "+puntos;
        elMarcador.html(marcador);
        
        // informar del progreso
        if (progressCallback && typeof progressCallback==="function") progressCallback.call(null);
    }
    
    function end() {
        // midi finalizado
        // ---auto stop---
        detenerJuego();
        elStartStop.removeClass("stop").addClass("start");
        juegoCompletado=true;
        
        // fin del juego : informar
        if (endCallback && typeof endCallback==="function") endCallback.call();
        
        tiempoInicio=null;
    }

    function detenerJuego() {
        gameInterface.midiPlayer.stop();
        elNotas.empty();
        running=false;
    }

    // --------------------------------------------------------------------------

    function primeraSecuencia() {
        faseMostrando=true;
        indexInicio=0;
        secuenciaNotas=[];
        if (params.secuencias.tipo=="random" || params.secuencias.tipo=="random-sec") {
            newRandom(false);
            playRandom();
        }
        else gameInterface.midiPlayer.play(createNote,params.velocidad,playNote,params.previsualizar);
    }
    
    function iniciarSecuencia() {
        // segun el tipo
        if (params.secuencias.tipo=="midi") {
            // repite (mantengo indexInicio=0)
            indexInicio=0;
            // creciendo en longitud
            secuenciaLongitud+=params.secuencias.crecer;
        } else if (params.secuencias.tipo=="midi-sec") {
            // continua (actualizo indexInicio)
            indexInicio=lastIndex+1;
            // con longitud fija de nuevas notas
            secuenciaLongitud=params.secuencias.crecer;
        } else if (params.secuencias.tipo=="random") {
            // creciendo en longitud
            secuenciaLongitud+=params.secuencias.crecer;
            // nueva secuencia
            newRandom(false);
        } else if (params.secuencias.tipo=="random-sec") {
            // creciendo en longitud
            secuenciaLongitud+=params.secuencias.crecer;
            // agregando a secuencia
            newRandom(true);
        }
            
        reiniciarSecuencia();   // lanza de nuevo
    }
    
    function reiniciarSecuencia() {
        secuenciaNotas=[];
        faseMostrando=true;
        if (params.secuencias.tipo=="random" || params.secuencias.tipo=="random-sec") playRandom();
        else {
            // mantener datos de secuencia (longitud, index inicio)
            gameInterface.midiPlayer.search(indexInicio);
            gameInterface.midiPlayer.resume();
        }
    }
    
    function finMidi() {
        // se nos ha acabado el midi,
        // pasar a repetir la frase
        faseMostrando=false;
        // y si ganas ya es la ultima
        params.secuencias.ganar=secuenciaAciertos+1;
    }
    
    var randomSeq=[];
    var randomIndex=0;
    function newRandom(agregar) {
        if (!agregar) randomSeq=[];
        while (randomSeq.length<secuenciaLongitud) {
            var key=params.lanzadores[Math.floor(Math.random()*params.lanzadores.length)].nota.toLocaleUpperCase();
            randomSeq.push(key);
        }
    }
    function playRandom() {
        randomIndex=0;
        randomKey();
    }
    function randomKey() {
        if (!running) return;
        // emular evento
        var e={type:"channel",
               subtype:"noteOn",
               track:1,
               noteNumber: gameInterface.midiPlayer.keyToNote[randomSeq[randomIndex]],
               figura: "negra",
               puntillo: false,
               index: randomIndex,
               channel: 0
              };
               
            // crear nota
                createNote(e);
        
            // sonar nota
            setTimeout(function(){if (running) playNote(e);}, params.previsualizar);
                
        
        // avanzar indice
        randomIndex++;
        // lanzar siguiente
        if (randomIndex<secuenciaLongitud) {
            setTimeout(randomKey,1000);
        }
    }
    
    function createNote(e) {
        // ignorar otras pistas, si se indica una soloTrack (NO SE DIBUJAN - PERO SI SONARAN)
        if (params.soloTrack!=null && e.track!=params.soloTrack) return;
        
        if (e.type=="channel" && e.subtype=="noteOn") {
            var altura="50";  
            var elNota = $(document.createElement("div")).addClass("fadlib capa nota");
            elNota.append(document.createElement("img"));
            
            if (params.notas==true) { // colcar figura y posicion
                altura=gameInterface.midiPlayer.noteToAlt[e.noteNumber];  
                
                if (e.figura!=null) elNota.addClass(e.figura);            

                if (e.modifier!=null) {
                    var tick=$(document.createElement("img")).addClass(e.modifier);
                    elNota.append(tick);
                }
                if (e.puntillo) {
                    var punt=$(document.createElement("img")).addClass("puntillo");
                    elNota.append(punt);
                }
            }
            elNota.css("top",altura+"%");
            
            // animar nota (de derecha a izquierda)
            elNota.css("left","110%").animate({left:"50%"},
                                            {duration:params.previsualizar, easing:"linear",
                                             complete:function(){$(this).addClass("resaltar")}
                                            }).animate({left:"-10%"},
                                            {duration:500, easing:"linear",
                                             complete:function(){$(this).remove()}
                                            });
            
            elNotas.append(elNota);
            
            // almacenar como siguiente nota en la secuencia
            secuenciaNotas.push(e);
            lastIndex=e.index;
            
            // hasta llegar a la longitud de secuencia actual
            if (secuenciaNotas.length==secuenciaLongitud) {
                gameInterface.midiPlayer.pause();
                //alert(lastIndex);
            }
        }
        if (e.type=="channel" && e.subtype=="silence") {
            var elNota = $(document.createElement("div")).addClass("fadlib capa silencio");
            elNota.append(document.createElement("img"));
            if (e.figura!=null) elNota.addClass(e.figura);
            // animar nota-silencio (de derecha a izquierda)
            elNota.css("left","110%").animate({left:"50%"},
                                            {duration:params.previsualizar, easing:"linear",
                                             complete:function(){$(this).addClass("resaltar")}
                                            }).animate({left:"-10%"},
                                            {duration:500, easing:"linear",
                                             complete:function(){$(this).remove()}
                                            });
            elNotas.append(elNota);
        }
    }
    
    function playNote(e) {
        updateMarcador();
        
        // sonar solo la pistas del ejercicio si se indica una soloTrack
        // las del ejercicio (soloTrack) (SE DIBUJAN - Y SUENAN)
        // el resto (no soloTrack) NO SE DIBUJAN - NI SUENAN
        if (params.soloTrack!=null && e.track!=params.soloTrack) return;
        
        if (e.type=="channel" && e.subtype=="noteOn") {
            gameInterface.midiPlayer.defaultEvent(e,
                    (params.secuencias.tipo=="random" || params.secuencias.tipo=="random-sec") );
            // lo del final es para forzar a sonar, aunque el midi no esté en play
            // (las random  y random-sec no vienen de midi)
        } else gameInterface.midiPlayer.defaultEvent(e);
        
        // esperar al final de la secuencia actual - e iniciar fase de tocar
        if (secuenciaNotas.length==secuenciaLongitud) faseMostrando=false;
    }
    
     // -------------------- lanzadores
    function lanzadorTecla(e) {
        for (var i=0;i<lanzaTeclas.length; i++) {
            if (lanzaTeclas[i].tecla.toLocaleLowerCase()==String.fromCharCode(e.which).toLocaleLowerCase()) {
                lanzadorClick.call(lanzaTeclas[i].lanzador);
            }
        }
    }
    
    function lanzadorClick() {
        // solo activos en turno de tocar (no mostrar)
        if (faseMostrando) return;  
        
        var lanza=($(this).data("lanzador"));
        var instr=lanza.instrumento || 0;
        var instr=gameInterface.inst[instr];
        var nota=lanza.nota || "a4";
        
        
        var siguiente=secuenciaNotas[0];
        if (siguiente!=null) {
            // eliminar del pentagrama
            secuenciaNotas.shift();    // la sacamos de la lista de pendientes
            if (gameInterface.midiPlayer.noteToKey[siguiente.noteNumber]==nota.toUpperCase()) {
                // acertamos
                // sonar nota
                instr.play(nota,0,-1);
                
                // puntuacion
                aciertos++;
                puntos+=params.puntos.nota;
                
                if (secuenciaNotas.length==0) {
                    secuenciaAciertos++;
                    puntos+=params.puntos.secuencia;
                    
                    // ver si hemos ganado
                    if (params.secuencias.ganar==secuenciaAciertos) {
                        // fin del juego
                        end();
                    } else {
                        // continuar con otra secuencia
                        iniciarSecuencia();
                    }
                }
            } else {
                // fallada
                puntos+=params.puntos.fallo;
                fallos++;
                
                // sonar error
                gameInterface.inst[params.error.instrumento].play(params.error.nota,0,-1);
                
                reiniciarSecuencia();
            }
            updateMarcador();
        }
    }

    
    
    function metroCallback() {
        // metronomo TICK
        $(".fadlib#metronomo").addClass("beat").delay(500).queue(
            function(){$(this).removeClass("beat").dequeue();});
    }
    
    
    function compasCallback() {
            var elLinea = $(document.createElement("div")).addClass("fadlib capa compas");
            //elLinea.append(document.createElement("img"));
            //elLinea.css("top",altura+"%");
            
            // animar linea compass (de derecha a izquierda)
            elLinea.css("left","110%").animate({left:"50%"},
                                            {duration:params.previsualizar, easing:"linear",
                                             complete:function(){$(this).addClass("resaltar")}
                                            }).animate({left:"-10%"},
                                            {duration:500, easing:"linear",
                                             complete:function(){$(this).remove()}
                                            });

            elNotas.append(elLinea);
    }
    
    
    // ---------------------- game Data
    function getData() {
        if (tiempoInicio!=null) tiempoJuego=((new Date().getTime())-tiempoInicio)/1000; // tiempo en secs.
        
        return {
            secuencia: secuenciaLongitud,
            aciertos: aciertos,
            errores: fallos,
            puntos: puntos,
            tiempo: tiempoJuego,
            completado: juegoCompletado
        }
    }
    
    // ------------------- set Events Callback ----------------------------------
    var startCallback=null;
    function setStartCallback(callback) {
        startCallback=callback;
    }
    var endCallback=null;
    function setEndCallback(callback) {
        endCallback=callback;
    }
    var stopCallback=null;
    function setStopCallback(callback) {
        stopCallback=callback;
    }
    
    var progressCallback=null;
    function setProgressCallback(callback) {
        progressCallback=callback;
    }
    
    
    // ------------------- public -----------------------------------------------
    
    this.init=init;
    this.start=start;
    this.stop=stop;
    this.isRunning=isRunning;
    this.getData=getData;
    this.setStartCallback=setStartCallback;
    this.setEndCallback=setEndCallback;
    this.setStopCallback=setStopCallback;
    this.setProgressCallback=setProgressCallback;

    return this;
}
    