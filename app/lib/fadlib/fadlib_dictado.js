FAdLib_Game = function(_mainDiv, _params, _gameInterface) {
    console.log(":: FAdlib : Juego Dictado ::");   
    
    var running=false;
    
    var mainDiv=_mainDiv;
    var params=_params;
    var gameInterface=_gameInterface;
    
    //parametros basicos (requeridos) : valor por defecto
    var paramsDefault={
        pentagrama: true,
        notas: true,
        velocidad: 100,
        previsualizar: 2000,
        soloTrack: null,
        puntos: {
            maximo: 100,
            medio: 50,
            minimo: 25,
            fallo: -50,
            ignorada: 0
        },
        margenes: {
            maximo: 75,
            medio: 50
        },
        error: {
            nota: "a3",
            instrumento: 0
        }
    }
    for (var i in paramsDefault) {
        if (params[i]==undefined) params[i]=paramsDefault[i];
    }
    
    // -----------------------------------------------------------------------------
    
    var htmlLayout=`
    <div class="fadlib full vertical" id="fondo">
        <div class="fadlib horizontal full" id="cabecera">
            <div class="fadlib" id="controles">
                <img class="fadlib start" id="startstop" />
            </div>
            <div class="fadlib center" id="metronomo"></div>
            <div class="fadlib" id="marcadores">
                <div class="fadlib right" id="marcador">Marcadores</div>
            </div>
        </div>
        <div class="fadlib canvas" id="juego">
            <img class="fadlib capa pentagrama" />
            <img class="fadlib capa vcenter clave" />
            <div class="fadlib capa vcenter compasText" id="compasText" />
            <img class="fadlib capa cabezalectora hcenter" />
            <img class="fadlib capa cabezamedio hcenter" />
            <img class="fadlib capa cabezaminimo hcenter" />
            <div class="fadlib capa full" id="notas"></div>
        </div>
        <div class="fadlib center vcenter" id="lanzadores"></div>
    </div>
    `;

    //      fadlib capa vcenter 
    // -----------------------------------------------------------------------------
    
    function isRunning() {
        return running;
    }
    
    var elJuego=null;
    var elNotas=null;
    var elMarcador=null;
    var elStartStop=null;
    var elMargenMedio=null;
    var elMargenMinimo=null
    var lanzaTeclas=[];
    function init() {
        // validar parametros (valores minimos)
        if (params.velocidad<1) params.velocidad=1;
        if (params.previsualizar<1) params.previsualizar=1;
        
        // crear elementos visuales
        
        mainDiv.append(htmlLayout);
        
        //elJuego = $(".fadlib#juego");
        elNotas = $(".fadlib#notas");
        elMarcador = $(".fadlib#marcador");
        
        if (params.pentagrama==false) $(".fadlib.pentagrama").hide();
        if (params.notas==false || params.pentagrama==false) $(".fadlib.clave").hide();
        
        // ajustar posicion de los margenes
        elMargenMedio= $(".fadlib.cabezamedio");
        console.log((50+params.margenes.medio*0.5)+"%");
        elMargenMedio.css("left",(50+(100-params.margenes.maximo)*0.5)+"%");
        elMargenMinimo=$(".fadlib.cabezaminimo");
        elMargenMinimo.css("left",(50+(100-params.margenes.medio)*0.5)+"%");
        
        // crear lanzadores
        if (params.lanzadores!=undefined && params.lanzadores instanceof Array) {
            lanzaTeclas=[];
            $(document).keydown(lanzadorTecla);
            var elLanzadores=$(".fadlib#lanzadores");
            for (var index in params.lanzadores) {
                var l=params.lanzadores[index];
                if (l!=null) {
                    var elLanza=$(document.createElement("img")).addClass("fadlib").addClass("lanzador");
                    if (l.class!=undefined) elLanza.addClass(l.class);
                    elLanzadores.append(elLanza);
                    var nota=l.nota || "a4";
                    var instrumento=l.instrumento || 0;
                    elLanza.data("lanzador", {nota:nota, instrumento:instrumento});
                    elLanza.click(lanzadorClick);
                    if (l.tecla!=undefined) {
                        lanzaTeclas.push({tecla:l.tecla, lanzador:elLanza});
                    }
                }
            }
        }
        
        if (!gameInterface.midiPlayer.isReady()) gameInterface.showError("Dictado: necesito un archivo MIDI");
        
        // metronomo
        if (params.metronomo!=undefined) {
            gameInterface.midiPlayer.setMetronome(params.metronomo.instrumento, params.metronomo.nota, params.metronomo.step,params.metronomo.prestep);
            gameInterface.midiPlayer.setMetronomeCallback(metroCallback);
        } else $(".fadlib#metronomo").hide();
        
        // compas
        if (params.compas!=undefined) {
            var compas=params.compas.split("/");
            gameInterface.midiPlayer.setCompas(compas[0],compas[1]);
            gameInterface.midiPlayer.setCompasCallback(compasCallback);
            $(".fadlib#compasText").html(compas[0]+'</br>'+compas[1]);
        }
        
        // detectar silencios
        if (params.soloTrack!=undefined && params.canalSilencios!=undefined) {
            gameInterface.midiPlayer.detectarSilencios(params.soloTrack, params.canalSilencios);
        }
        
        elStartStop=$(".fadlib#startstop").click(function(e){
            if (running) stop(); else start();
        });
        
        updateMarcador();        
    }
    
    
    var tiempoInicio=null;
    var tiempoJuego=0;
    var juegoCompletado=false;
    var aciertos=0;
    var fallos=0;
    var puntos=0;
    
    function start() {
        if (running) return;
        
        running=true;
        elStartStop.removeClass("start").addClass("stop");
        
        gameInterface.midiPlayer.setEndCallback(end);
        
        tiempoInicio=new Date().getTime();  // miliseconds clock
        tiempoJuego=0;
        juegoCompletado=false;
        aciertos=0;
        fallos=0;
        puntos=0;
        notasPendientes=[];
        
        gameInterface.midiPlayer.play(createNote,params.velocidad,playNote,params.previsualizar);

        updateMarcador();
        // informar de que el juego se ha iniciado -por usuario o auto-
        if (startCallback && typeof startCallback==="function") startCallback.call();
    }
    
    
    function stop() {
        if (!running) return;
        
        // detener el midi
        detenerJuego();
        elStartStop.removeClass("stop").addClass("start");
        juegoCompletado=false;
        
        // informar de que el usuario lo ha detenido -manualmente-
        if (stopCallback && typeof stopCallback==="function") stopCallback.call();
        
        tiempoInicio=null;
    }
    
    
    function updateMarcador() {
        // mostrar marcador, progreso realizado
        var p=gameInterface.midiPlayer.getProgress(true);
        var t=gameInterface.midiPlayer.getTotalLength();
        var marcador=p+"/"+t+"   ("+Math.round(100*p/t)+"%)"+
            `</br>`+"aciertos = "+aciertos+
            `</br>`+"fallos = "+fallos+
            `</br>`+"puntos = "+puntos;
        elMarcador.html(marcador);
        //elMarcador.text(p+"/"+t+"   ("+Math.round(100*p/t)+"%)");
        
        // informar del progreso
        if (progressCallback && typeof progressCallback==="function") progressCallback.call(null);
    }
    
    function end() {
        // midi finalizado
        // ---auto stop---
        detenerJuego();
        elStartStop.removeClass("stop").addClass("start");
        juegoCompletado=true;
        
        // fin del juego : informar
        if (endCallback && typeof endCallback==="function") endCallback.call();
        
        tiempoInicio=null;
    }
    
    function detenerJuego() {
        gameInterface.midiPlayer.stop();
        elNotas.empty();
        running=false;
    }
    
    // -------------------- lanzadores
    function lanzadorTecla(e) {
        for (var i=0;i<lanzaTeclas.length; i++) {
            if (lanzaTeclas[i].tecla.toLocaleLowerCase()==String.fromCharCode(e.which).toLocaleLowerCase()) {
                lanzadorClick.call(lanzaTeclas[i].lanzador);
            }
        }
    }
    
    function lanzadorClick() {
        var lanza=($(this).data("lanzador"));
        var instr=lanza.instrumento || 0;
        var instr=gameInterface.inst[instr];
        var nota=lanza.nota || "a4";
        
        
        var siguiente=notasPendientes[0];
        if (siguiente!=null) {
            // eliminar del pentagrama
            notasPendientes.shift();    // la sacamos de la lista de pendientes
            if (gameInterface.midiPlayer.noteToKey[siguiente.event.noteNumber]==nota.toUpperCase()) {
                // acertamos
                siguiente.element.addClass("resaltar");
                // sonar nota
                instr.play(nota,0,-1);
                
                // puntuacion
                if (siguiente.element.position().left>elMargenMinimo.position().left)
                    puntos+=params.puntos.minimo;
                else if (siguiente.element.position().left>elMargenMedio.position().left)
                    puntos+=params.puntos.medio;
                else puntos+=params.puntos.maximo;
                
                aciertos++;
                updateMarcador();
            } else {
                // fallada
                puntos+=params.puntos.fallo;
                notaFallada(siguiente); // sonará error
            }
        }
    }
    
    var notasPendientes=[];
    function createNote(e) {
        // ignorar otras pistas, si se indica una soloTrack (NO SE DIBUJAN - PERO SI SONARAN)
        if (params.soloTrack!=null && e.track!=params.soloTrack) return;
        
        if (e.type=="channel" && e.subtype=="noteOn") {
            var altura="50";  
            var elNota = $(document.createElement("div")).addClass("fadlib capa nota");
            elNota.append(document.createElement("img"));
            
            if (params.notas==true) { // colcar figura y posicion
                altura=gameInterface.midiPlayer.noteToAlt[e.noteNumber];  
                
                if (e.figura!=null) elNota.addClass(e.figura);            

                if (e.modifier!=null) {
                    var tick=$(document.createElement("img")).addClass(e.modifier);
                    elNota.append(tick);
                }
                if (e.puntillo) {
                    var punt=$(document.createElement("img")).addClass("puntillo");
                    elNota.append(punt);
                }
            }
            elNota.css("top",altura+"%");
            
            // animar nota (de derecha a izquierda)
            elNota.css("left","110%").animate({left:"50%"},
                                            {duration:params.previsualizar, easing:"linear",
                                             complete: notaPasada});
            elNotas.append(elNota);
            
            // almacenar como siguiente nota a pulsar
            notasPendientes.push({event:e, element:elNota});
        }
        if (e.type=="channel" && e.subtype=="silence") {
            var elNota = $(document.createElement("div")).addClass("fadlib capa silencio");
            elNota.append(document.createElement("img"));
            if (e.figura!=null) elNota.addClass(e.figura);
            // animar nota-silencio (de derecha a izquierda)
            elNota.css("left","110%").animate({left:"50%"},
                                            {duration:params.previsualizar, easing:"linear",
                                             complete:function(){$(this).addClass("resaltar")}
                                            }).animate({left:"-10%"},
                                            {duration:500, easing:"linear",
                                             complete:function(){$(this).remove()}
                                            });
            elNotas.append(elNota);
        }
        
    }
    
    function notaPasada() {
        if (!running) return;
        var siguiente=notasPendientes[0];

        if (siguiente!=null) {
            if (siguiente.element[0]==this) {
                // ha llegado lo siguiente nota a la cabeza, por tanto, se ha fallado
                puntos+=params.puntos.ignorada;
                notaFallada(siguiente);
                notasPendientes.shift();    // sacar la nota...
            } else {
                // ha llegado una nota, pero no era la que esperabamos
                // es una nota ya acertada (eliminada de la lista)
                // IGNORARLA
            }
        }
        
        
    }
    
    function notaFallada(siguiente) {
        siguiente.element.addClass("fallada");
        siguiente.element.animate({left:"-10%"},
                        {duration:500, easing:"linear",
                        complete:function(){$(this).remove()}
                        });
        // sonar error
        gameInterface.inst[params.error.instrumento].play(params.error.nota,0,-1);
        
        // puntuacion
        fallos++;
        updateMarcador();
    }
    
    
    // ----- play midi events
    function playNote(e) {
        updateMarcador();
        
        // ignorar la pistas del ejercicio si se indica una soloTrack
        // las del ejercicio (soloTrack) (SE DIBUJAN - PERO NO SUENAN)
        // el resto (no soloTrack) NO SE DIBUJAN - PERO SUENAN
        if (params.soloTrack!=null && e.track==params.soloTrack) return;
        
        if (e.type=="channel" && e.subtype=="noteOn") {
            gameInterface.midiPlayer.defaultEvent(e);
        } else gameInterface.midiPlayer.defaultEvent(e);
    }
    
    
    
    // ---------------------- game Data
    function getData() {
        if (tiempoInicio!=null) tiempoJuego=((new Date().getTime())-tiempoInicio)/1000; // tiempo en secs.
        
        return {
            aciertos: aciertos,
            errores: fallos,
            puntos: puntos,
            tiempo: tiempoJuego,
            completado: juegoCompletado
        }
    }
    
    function metroCallback() {
        // metronomo TICK
        $(".fadlib#metronomo").addClass("beat").delay(500).queue(
            function(){$(this).removeClass("beat").dequeue();});
    }
    
    function compasCallback() {
            var elLinea = $(document.createElement("div")).addClass("fadlib capa compas");
            //elLinea.append(document.createElement("img"));
            //elLinea.css("top",altura+"%");
            
            // animar linea compass (de derecha a izquierda)
            elLinea.css("left","110%").animate({left:"50%"},
                                            {duration:params.previsualizar, easing:"linear",
                                             complete:function(){$(this).addClass("resaltar")}
                                            }).animate({left:"-10%"},
                                            {duration:500, easing:"linear",
                                             complete:function(){$(this).remove()}
                                            });

            elNotas.append(elLinea);
    }
    
    // ------------------- set Events Callback ----------------------------------
    var startCallback=null;
    function setStartCallback(callback) {
        startCallback=callback;
    }
    var endCallback=null;
    function setEndCallback(callback) {
        endCallback=callback;
    }
    var stopCallback=null;
    function setStopCallback(callback) {
        stopCallback=callback;
    }
    
    var progressCallback=null;
    function setProgressCallback(callback) {
        progressCallback=callback;
    }
    
    
    // ------------------- public -----------------------------------------------
    
    this.init=init;
    this.start=start;
    this.stop=stop;
    this.isRunning=isRunning;
    this.getData=getData;
    this.setStartCallback=setStartCallback;
    this.setEndCallback=setEndCallback;
    this.setStopCallback=setStopCallback;
    this.setProgressCallback=setProgressCallback;

    return this;
}
    