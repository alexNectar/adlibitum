function loadRemote(path, callback, callbackError) {
    var fetch = new XMLHttpRequest();
    fetch.open('GET', path);
    fetch.overrideMimeType("text/plain; charset=x-user-defined");
    fetch.onreadystatechange = function() {
        if(this.readyState == 4) {
            if (this.status == 200) {
                /* munge response into a binary string */
                var t = this.responseText || "" ;
                var ff = [];
                var mx = t.length;
                var scc= String.fromCharCode;
                for (var z = 0; z < mx; z++) {
                    ff[z] = scc(t.charCodeAt(z) & 255);
                }
                callback(ff.join(""));
            } else callbackError();
        }
    }
    fetch.send();
}