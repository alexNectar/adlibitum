FMidi = function(instrumentos) {
    
    // static config
    var useBemol=false;
        
    // internal data
    var eventsBuffer=[];
    var midiFile=null;
    var midiPlaying=false;
    var midiPause=false;
    var currentPlay=-1;
    var progress=0;
    var delayedProgress=0;
    var totalLength=0;
    var ready=false;
    
    var inst=instrumentos;
    
    function isReady() {
        return ready;
    }
    
    function prepareMidi(file, callback) {
        log("------ prepare Midi -----");
        
        // vaciar midi anterior
        ready=false;
        eventsBuffer=[];
        midiFile=null;
        metronomo=false;
        
        // cargar archivo midi desde ubicacion externa
        loadRemote(file, function(data) {
            midiFile = MidiFile(data);

            log("tracks: "+midiFile.header.trackCount);

            var ntracks=midiFile.header.trackCount;
            var ticksPerBeat=midiFile.header.ticksPerBeat;
            
            var nextTrackEvent=[], nextTrackTime=[];
            var currentTime=0;
            for (var t=0;t<ntracks;t++) {
                nextTrackEvent[t]=0;
                nextTrackTime[t]=0;
                //log("   track #"+t+" = "+midiFile.tracks[t].length);
            }
            var noteOnTime=[], noteOnIndex=[];
            for (var c=0;c<16;c++) {noteOnTime[c]=[];noteOnIndex[c]=[];}

            totalLength=0;
            
            do {
                var minTime=10000000;
                var nextEvent=null, nextTrack=null;
                for (var t=0;t<ntracks;t++) {
                    if (nextTrackEvent[t]<midiFile.tracks[t].length) {
                        var castEvent=midiFile.tracks[t][nextTrackEvent[t]];
                        var castTime=nextTrackTime[t]+castEvent.deltaTime;
                        if (castTime<minTime) {
                            minTime=castTime;
                            nextEvent=castEvent;
                            nextTrack=t;
                        }
                    }
                }
                if (nextEvent!=null) {
                    // point to next event on track
                    nextTrackEvent[nextTrack]++;
                    var targetTime=nextTrackTime[nextTrack]+nextEvent.deltaTime;
                    nextTrackTime[nextTrack]=targetTime;
                    nextEvent.deltaTime=targetTime-currentTime;
                    currentTime=targetTime;
                    
                    // find initial tempo
                    if (initialMsPerBeat==0 && nextEvent.type=="meta" && nextEvent.subtype=="setTempo")
                        initialMsPerBeat=(nextEvent.microsecondsPerBeat/1000);
                    
                    // calculate note duration
                    if (nextEvent.type=="channel") {
                        if (nextEvent.subtype=="noteOn") {
                            totalLength++;
                            noteOnIndex[nextEvent.channel][nextEvent.noteNumber]=eventsBuffer.length;
                            noteOnTime[nextEvent.channel][nextEvent.noteNumber]=currentTime;
                            nextEvent.track=nextTrack;
                            // por si no tiene su noteOff correspondiente
                            nextEvent.duration=ticksPerBeat;    // default 1 beat
                            nextEvent.beats=32;                 // default 32 sub-beats (negra)
                            nextEvent.figura="negra";
                            nextEvent.puntillo=false;
                            nextEvent.modifier=null;
                        }
                        if (nextEvent.subtype=="noteOff") {
                            nextEvent.track=nextTrack;
                            var eventNoteOnIndex=noteOnIndex[nextEvent.channel][nextEvent.noteNumber];
                            var timeOn=noteOnTime[nextEvent.channel][nextEvent.noteNumber];
                            eventNoteOn=eventsBuffer[eventNoteOnIndex];
                            eventNoteOn.duration=(currentTime-timeOn);
                            eventNoteOn.beats=((eventNoteOn.duration/ticksPerBeat)*32);
                            // 1 beat = 32 parts (up to 1part=semifusa)
                            getEventTempo(eventNoteOn);
                        }
                    }

                    /*
                    // MOSTRAR QUE ELEMENTOS SE LEEN DE UNA NOTA
                    // deltaTime, channel, type, subtype
                    // track, noteNumber, velocity
                    // duration, beats, figura, puntillo, modifier
                    if (nextEvent.type=="channel") {
                        var datos="";
                        for (d in nextEvent) datos+=(d+", ");
                        alert(datos);
                        return;
                    }
                    //*/
                    
                    // add next (modified) event to eventsBuffer
                    eventsBuffer.push(nextEvent);
                }
            } while (nextEvent!=null);
            
            log("events: "+eventsBuffer.length);
            
            if (midiFile!=null && eventsBuffer!=null && eventsBuffer.length>0) ready=true;
            
            // launch callback function
            if (callback && typeof callback === "function") callback.call();

        }, function(){
            alert("error cargando midi");
        });
    }
    
    var timeOut=null;
    var currentIndex=0;
    var ticksPerBeat=0;
    var initialMsPerBeat=0;
    var msPerBeat=0;
    var currentSpeed=1
    var onMidiEvent=null;
    var onDelayEvent=null;
    var timeDelay=null;
    function playMidi(_onMidiEvent, speed, _onDelayEvent, _timeDelay) {
        //log("----- play midi ----");
        //log("eventsBuffer.lenght = "+eventsBuffer.length);

        if (midiPlaying || eventsBuffer.length<=0 || !midiFile) return;

        onMidiEvent=_onMidiEvent;
        onDelayEvent=_onDelayEvent;
        timeDelay=_timeDelay;
        
        
        currentPlay++;
        midiPlaying=true;
        midiPause=false;
        progress=0;
        delayedProgress=0;
        log("::: START ::: "+currentPlay);
        currentIndex=0;
        ticksPerBeat=midiFile.header.ticksPerBeat;
        msPerBeat=initialMsPerBeat;
        
        currentSpeed=1;
        if (speed!=0) currentSpeed=(100/speed);
        
        timeOut=setTimeout(function(){nextEvent(currentPlay)}, 0);
        metronomePlay();
        compasAcum=0;
    }
    
    function resume() {
        if (!midiPause || eventsBuffer.length<=0 || !midiFile) return;
        currentPlay++;
        midiPlaying=true;
        midiPause=false;
        log("::: RESUME ::: "+currentPlay);
        timeOut=setTimeout(function(){nextEvent(currentPlay)}, 0);
        metronomePlay();
    }
    
    function search(index) {
        compasAcum=0;
        if (index<eventsBuffer.length) currentIndex=index;
    }
    
    function nextEvent(thisPlay) {
        if (midiPlaying && !midiPause && thisPlay==currentPlay) {  // mute event si se quiere parar el play
            var event=eventsBuffer[currentIndex];
            if (event==undefined) return;   // delayed-event when game-deleted, midifile-changed

            event.index=currentIndex;
            
            if (event.type=="channel" && event.subtype=="noteOn") progress++;

            // apply setTempo events
            if (event.type=="meta" && event.subtype=="setTempo") {
                msPerBeat=(event.microsecondsPerBeat/1000);
            }
            
            // compas line check
            compasAcum+=((event.deltaTime*compasDiv)/(this.ticksPerBeat*4));
            //console.log("fmidi compass: ("+compasTop+"/"+compasDiv+") ="+compasAcum);
            while (compasAcum>=compasTop) {
                if (compasCallback && typeof compasCallback==="function") compasCallback.call();
                compasAcum-=compasTop;
            }

            // launch event
            if (onMidiEvent && typeof onMidiEvent === "function")
                onMidiEvent.call(null,event);

            // prepare delay event
            if (onDelayEvent && typeof onDelayEvent==="function" && timeDelay>0) {
                setTimeout(function(){
                    if (midiPlaying && thisPlay==currentPlay) {
                        if (event.type=="channel" && event.subtype=="noteOn") delayedProgress++;
                        onDelayEvent.call(null,event);
                    }
                }, timeDelay);
            }

            // take next event (solo si aun playing)
            currentIndex++;
            if (midiPlaying && thisPlay==currentPlay && currentIndex<eventsBuffer.length) {
                if (!midiPause) {
                    timeOut=setTimeout(function(){nextEvent(thisPlay)}, ticksToMs(eventsBuffer[currentIndex].deltaTime));
                }
            } else {
                midiPlaying=false;
                log("::: END :::");
                if (endCallback && typeof endCallback==="function") endCallback.call();
            }
        }
    }

    function ticksToMs(ticks) {
        return (ticks*msPerBeat/ticksPerBeat)*currentSpeed;
    }
    
    
    function defaultMidiEvent(e, forcePlay) {
        if (!midiPlaying && !forcePlay) return;
        if (e.type=="channel") {
            if (e.subtype=="noteOn") {
                var i=inst[e.channel];
                if (inst.length==1) i=inst[0];
                if (i) i.play(noteToKey[e.noteNumber],0,-1);
                //channelNotes[e.channel][e.noteNumber]=i.play(noteToKey[ e.noteNumber],0,-1);
            } else if (e.subtype=="noteOff") {
                //channelNotes[e.channel][e.noteNumber].stop(0);
            }
        }
    }
    
    function stopMidi() {
        if (midiPlaying) {
            clearTimeout(timeOut);
            metronomeStop();
            midiPlaying=false;
        }
    }
    
    function pauseMidi() {
        if (midiPlaying) {
            // no mato el timeOut... para que llegue a sonar
            midiPause=true;
        }
    }
    
    function getProgress(delayed) {
        if (!midiPlaying) return 0;
        if (delayed==true) {
            return delayedProgress;
        } else return progress;
    }
    
    function getTotalLength() {
        return totalLength;
    }
    
    function getEventTempo(e) {
            var figura=null;
            var puntillo=false;
            
            if (e.beats>192-32) {
                figura="redonda";
                puntillo=true;
            } else if (e.beats>128-16) {
                figura="redonda";
            } else if (e.beats>96-16) {
                figura="blanca";
                puntillo=true;
            } else if (e.beats>64-8) {
                figura="blanca";
            } else if (e.beats>48-8) {
                figura="negra";
                puntillo=true;
            } else if (e.beats>32-4) {
                figura="negra";
            } else if (e.beats>24-4) {
                figura="corchea";
                puntillo=true;
            } else if (e.beats>16-2) {
                figura="corchea";
            } else if (e.beats>12-2) {
                figura="semicorchea";
                puntillo=true;
            } else if (e.beats>8-1) {
                figura="semicorchea";
            } else if (e.beats>6-1) {
                figura="fusa";
                puntillo=true;
            } else if (e.beats>=4) {
                figura="fusa";
            } else if (e.beats>=3) {
                figura="semifusa";
                puntillo=true;
            } else if (e.beats>2-1) {
                figura="semifusa";
            }
        
            e.figura=figura;
            e.puntillo=puntillo;
        
            if (noteToKey[e.noteNumber].length>2) {
                if (noteToKey[e.noteNumber][1]=="b")
                    e.modifier="bemol";
                else e.modifier="sostenido";
            }
    }
    
    
    
    // note conversions
    var keyToNote = {}; // C8  == 108
    var noteToKey = {}; // 108 ==  C8
    var noteToAlt = {};  // F5 == 0%
    {
        var A0 = 0x15; // first note
        var C8 = 0x6C; // last note
        var pentaBase = 54;    // note on base of pentagram
        var number2keySost = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'];
        var number2keyBemol = ['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B'];
        var number2key=(useBemol)?number2keyBemol:number2keySost;
        var basekey=['C','D','E','F','G','A','B'];
        var altIni=100*(pentaBase-A0)/8, altStep=100/8;
        for (var n = A0; n <= C8; n++) {
            var octave = (n - 12) / 12 >> 0;
            var name = number2key[n % 12] + octave;
            keyToNote[name] = n;
            noteToKey[n] = name;

            //if (name.length>2) alt+=altStep; // ignore bemol
            //noteToAlt[n]=alt;
        }
        var basePenta=400+6*12.5;   // posicion en el pentagrama de la nota A0
        for (var n=A0; n<=C8; n++) {
            var octave = (n - 12) / 12 >> 0;
            var noteBaseName = number2key[n%12][0];
            var altOctv=-(octave*7*12.5);   // altura por octavas completas
            var altNote=-(basekey.indexOf(noteBaseName)*12.5);  // altura por nota dentro de la octava  
            noteToAlt[n]=basePenta+(altNote+altOctv);
        }
    };
    
    
    // local log
    function log(text) {
        // console.log(text);
    }
    
    
    // ---------------------------------- metronomo ---------------------
    var metronomo=false;
    var metroInst=null;
    var metroNota="a0";
    var metroStep=1;
    var metroPreStep=4;
    var metroTimer=null;
    var metroCallback=null;
    function setMetronome(instrumento, nota, step, presteps) {
        if (instrumento!=undefined) {
            metroInst=inst[instrumento];
            if (inst.length==1) metroInst=inst[0];
        }
        if (nota!=undefined) metroNota=nota;
        if (step!=undefined) metroStep=step;
        if (presteps!=undefined) metroPreStep=presteps;
        metronomo=true;
    }
    function setMetronomeCallback(callback) {
        metroCallback=callback;
    }
    function metronomePlay() {
        var iniciarEn=(timeDelay-ticksToMs((metroPreStep)*ticksPerBeat));
        if (iniciarEn<0) iniciarEn=timeDelay;
        if (metronomo==true) 
            metroTimer=setTimeout(metronomeTick, iniciarEn);
    }
    function metronomeStop() {
        clearTimeout(metroTimer);
    }
    function metronomeTick() {
        if (metroInst) metroInst.play(metroNota,0,-1);
        if (metroCallback && typeof metroCallback === "function") metroCallback.call();
        
        if (midiPlaying && !midiPause) {
            metroTimer=setTimeout(metronomeTick, ticksToMs(metroStep*ticksPerBeat));
        }
    }
    
    // --------------------------------- compas -----------------------------
    var compas=false;
    var compasTop=4;
    var compasDiv=4;
    var compasAcum=0;
    var compasCallback=null;
    function setCompas(top, div) {
        compasTop=top;
        compasDiv=div;
        compas=true;
    }
    function setCompasCallback(callback) {
        compasCallback=callback;
    }
    
    // --------------------------------- silencios -----------------------------
    function detectarSilencios(track, canal) {
        if (!midiFile) return;  // solo si tenemos midi cargado
        
        var tiempoActual=0;
        var lastNoteOffIndex=-1;
        var lastNoteOffTime=0;
        var notasActivas=0;
        var ticksPerBeat=midiFile.header.ticksPerBeat;
        for (var i=0;i<eventsBuffer.length;i++) {
            var event=eventsBuffer[i];
            tiempoActual+=event.deltaTime;  // avanzar tiempo
            // atendiendo al canal indicado
            if (event.type=="channel" && event.channel==canal && event.track==track) {
                // busco evento noteOff (guardo tiempo)
                if (event.subtype=="noteOff") {
                    lastNoteOffIndex=i;
                    lastNoteOffTime=tiempoActual;
                    notasActivas--;
                }
                // y siguiente noteOn
                if (event.subtype=="noteOn") {
                    // si tuvimos alguna "noteOff", y es la ulima activa que queda.
                    if (lastNoteOffIndex!=-1 && notasActivas==0) {
                        // si la diferencia es relevante (evitar los microsilencios) (>16beats = al menos... corchea=1/8)
                        if ((tiempoActual-lastNoteOffTime)>16) {
                            // crear evento silencio (en el noteOff anterior) con la duracion del silencio
                            // type="channel" subtype="silence"
                            // con delta del noteOff anterior
                            var lastEvent=eventsBuffer[lastNoteOffIndex];
                            var eventoSilencio={
                                type:"channel",
                                subtype:"silence",
                                deltaTime: lastEvent.deltaTime,
                                track: lastEvent.track,
                                duration: (tiempoActual-lastNoteOffTime),
                                beats: ((tiempoActual-lastNoteOffTime)/ticksPerBeat)*32,
                                noteNumber: 21
                            };
                            getEventTempo(eventoSilencio);  //asigna figura, puntillo [...y modifier(bemol/sost)]                
                            // noteOffanterior delta = 0 (lanza inmediatamente despues este silencio)
                            eventsBuffer[lastNoteOffIndex].deltaTime=0
                            // insertar evento silencio TRAS el noteOff
                            eventsBuffer.splice(lastNoteOffIndex,0,eventoSilencio);  //(index, eliminar=0, insertar 1)
                            i++;    // adelantar indice, tras la insercion (saltarnos el evento silencio y el ya analizado)
                        }
                    }
                    notasActivas++; // levar la cuentas de notas activas
                }
            }
        }
    }
    
    // --------------------------------- events callbacks ---------------
    var endCallback=null;
    function setEndCallback(callback) {
        endCallback=callback;
    }
    
    
    // --------------------------------------- public interface
    
        this.isReady=       isReady;
        this.prepare=       prepareMidi;
        this.play=          playMidi;
        this.stop=          stopMidi;
        this.pause=         pauseMidi;
        this.resume=        resume;
        this.search=        search;
    
        this.defaultEvent=  defaultMidiEvent;
        this.noteToKey=     noteToKey;
        this.keyToNote=     keyToNote;
        this.noteToAlt=     noteToAlt;
        this.getProgress=   getProgress;
        this.getTotalLength=getTotalLength;
        
        this.setEndCallback=setEndCallback;
        this.setMetronome=setMetronome;
        this.setMetronomeCallback=setMetronomeCallback;
        
        this.setCompas=setCompas;
        this.setCompasCallback=setCompasCallback;
    
        this.detectarSilencios=detectarSilencios;
    
    return this;
}