FAdLib = function() {

    // private
    // ---------------------params (default values)
    var paramsDefault={
        instrumentos: "banjo", //"celesta", //"acoustic_grand_piano",
    };
    
    
    function clearParams() {
        params={};
    }
    
    function setParamsDefault() {
        params=paramsDefault;
    }
    
    function addParamsDefault() {
        // aplicar parametros por defecto
        for (var i in paramsDefault) {
            if (params[i]==undefined) params[i]=paramsDefault[i];
        }
    }
    
    function setParams(parametros) {
        if (typeof parametros === "string") {
            try {
            parametros=JSON.parse(parametros);
            } catch(e) {alert("Error parsing JSON: \n"+e);}
        }
        if (typeof parametros === "object") {
            params=parametros;
        }
    }
    
    function addParams(parametros) {
        for (var i in parametros) {
            if (params[i]==undefined) params[i]=parametros[i];
        }
    }
    
    function getParams() {
        return params;
    }
    
    function setParam(parametro, valor) {
        if (parametro!=null) params[parametro]=valor;
    }
    
    function getParam(parametro) {
        if (parametro!=null)
            return params[parametro];
        else return null;
    }
    
    function clearParam(parametro) {
        setParam(parametro, null);
    }
    
    function addParam(parametro, valor) {
        if (parametro!=null) {
            if (params[parametro]!=null) {
                params[parmetro]=[].concat(params[parametro]).concat(valor);
            } else {
                setParam(parametro, valor);
            }
        }
    }
    
    // ---------------------public functions
    
    var ready=false;
    
    var customData=null;
    
    var currentPath=null;
    var dataPath="";
    var readyCallback=null;
    
    var css=[];
    var externaljs=false;
    var externaljs_loading=0;
    
    var midiPlayer=null;
    
    var game=null;
    var gamejs=null;
    
    var ctx=null;
    var soundfont=null;
    var instrumentsPath=null;
    var inst=[];
    var instReady=false;
    var inst_loading=0;
    
    var mainDiv=null;
    
    function init(callback) {
        
        if (callback && typeof callback==="function") readyCallback = callback;
        ready=false;
        
        // --------------- RESET ----- eliminar referencias antiguas
        // clear previous CSSs loaded
        for (var c in css) {
            document.getElementsByTagName("head")[0].removeChild(css[c]);
        }
        css=[];
        // clear previous game js link
        if (gamejs) {
            document.getElementsByTagName("head")[0].removeChild(gamejs);
        }
        gamejs=null;
        
        // clear game instance
        if (game!=null) game.stop();
        game=null;
        
        // clear game elements
        if (mainDiv!=null) mainDiv.empty();
        // -------------- RESET ----------------------------------------
        
        /* // EJECUTADO AL CARGAR ESTE SCRIPT (antes del init)
        if (!currentPath) currentPath=scriptPath();
        // ---------- cargar dependencias js
        if (!externaljs && externaljs_loading==0) { // si no cargados y no estan cargandose
            loadjscssfile(currentPath+"stream.js","js");
            loadjscssfile(currentPath+"midifile.js","js");
            loadjscssfile(currentPath+"loadRemote.js","js");
            loadjscssfile(currentPath+"soundfont-player.min.js","js");
            loadjscssfile(currentPath+"fmidi.js","js");
        }
        //*/
        
        if (params.dataPath!=undefined) dataPath=params.dataPath;
        gameInterface.dataPath=dataPath;
        
        // ----------- agregar JS segun tipo de juego
        if (params.game) {
            if (["canta","dictado","simon","test"].indexOf(params.game)!=-1) {
                gamejs=loadjscssfile(currentPath+"fadlib_"+params.game+".js","js");
            }
        } else return showError("Fallo Param 'game' = 'canta' 'dictado' 'simon' 'test'");
        
        if (mainDiv==null) mainDiv=$(document.createElement("div")).addClass("fadlib stage");
        // capturar mainDiv
        var d=null;
        if (params.mainDiv) {
            d=$(params.mainDiv);
            if (d!=null) d.append(mainDiv);
        }
        if (d==null) return showError("Fallo Param 'mainDiv' = jQuery.selector no valido")
        
        // ---------- css de estilo
        
        // load current CSSs
        var cssList=[].concat(params.css);
        if (cssList) {
            for (var cssIndex in cssList) {
                var fileref=loadjscssfile(dataPath+cssList[cssIndex], "css");
                css.push(fileref);
            }
        }
        
        if (externaljs_loading==0) init2();    // si pre-cargados... continuar inmediatamente
        
        return true;
    }
    
    function init2() {
        if (!externaljs) return;    // esperar a que terminen de cargar
        
        // (re-)iniciar libreria de audio
        if (ctx==null) ctx = new AudioContext();
        instrumentsPath=params.instrumentPath || currentPath+"local_instr/";
        var nameToUrl=function(instName) { return instrumentsPath+ instName + '-ogg.js';}
        if (soundfont==null) soundfont = new Soundfont(ctx,nameToUrl);
        else soundfont.nameToUrl=nameToUrl;
        
        // cargar instrumentos
        inst=[];    // eliminar instrumentos anteriores
        instReady=false;
        var instList=params.instrumentos || [];
        instList=[].concat(instList);   // convertir a array (aunque fuera solo un string)
        if (instList) {
            for (var instIndex in instList) {
                inst[instIndex]=soundfont.instrument(instList[instIndex]);
                inst_loading++;
                inst[instIndex].onready(function(){
                    inst_loading--;
                    if (inst_loading==0) {
                        instReady=true;
                        init3();
                    }
                });
            }
        }
        
        gameInterface.inst=inst;
        
        // crear instancia de fmidi
        if (midiPlayer==null) {
            midiPlayer=new FMidi(inst);
            gameInterface.midiPlayer=midiPlayer;
        }
        
        // crear instancia de juego
        if (gamejs) {
            game=new FAdLib_Game(mainDiv,params,gameInterface);
            game.setStartCallback(startCallback);
            game.setEndCallback(endCallback);
            game.setStopCallback(stopCallback);
            game.setProgressCallback(progressCallback);
        }
        
        if (inst.length==0) init3();    // si no hay instrumentos que cargar... continuar inmediatamente
        
    }

    function init3() {
        // opcionalmente (si es necesario) cargar y preprocesar archivo MIDI
        if (params.midiFile!=undefined && params.midiFile.length>0 && midiPlayer) {
            midiPlayer.prepare(dataPath+params.midiFile, initEnd);
        } else initEnd();
    }
    
    function initEnd() {

        if (params.customData!=undefined) customData=params.customData;
        
        if (game!=null) game.init();
        ready=true;
        if (readyCallback) readyCallback.call();
        
        // autostart
        if (params.autoStart && params.autoStart==true) game.start();
    }
    
    
    
    function isReady() {
        return ready;
    }
    
    // --------------------- game functions
    
    function start() {
        if (ready && (game && !game.isRunning())) {
            game.start();
            return true;
        } else return false;
    }
    
    function isRunning() {
        return (ready && (game && game.isRunning()));
    }
    
    function stop() {
        if (isRunning()) {
            if (midiPlayer!=null) midiPlayer.stop();
            if (game) game.stop();
        }
    }
    
        
    function getData() {
        return {
            customData: customData,
            gameData: game.getData()
        }
    }
    
    // ---------------------- events
    
    function startCallback() {
        if (eventStart && typeof eventStart==="function") eventStart.call(null);
    }
    
    function endCallback() {
        this.stop();
        if (eventEnd && typeof eventEnd==="function") eventEnd.call(null,getData());
    }
    
    function stopCallback() {
        this.stop();
        if (eventStop && typeof eventStop==="function") eventStop.call(null, getData());
    }
    
    function progressCallback() {
        if (eventProgress && typeof eventProgress==="function") eventProgress.call(null, getData());
    }
    
    // ----------------------------- tools
    
    function loadjscssfile(filename, filetype, inmediate){
        if (filetype=="js"){ //if filename is a external JavaScript file
            var fileref=document.createElement('script');
            fileref.setAttribute("type","text/javascript");
            fileref.setAttribute("src", filename);
            if (inmediate!=false) {
                externaljs_loading++;
                fileref.onload=function() {
                    externaljs_loading--;
                    if (externaljs_loading==0) {
                        externaljs=true;
                        init2();
                    };
                };
            }
        }
        else if (filetype=="css"){ //if filename is an external CSS file
            var fileref=document.createElement("link")
            fileref.setAttribute("rel", "stylesheet")
            fileref.setAttribute("type", "text/css")
            fileref.setAttribute("href", filename)
        }
        if (typeof fileref!="undefined")
            document.getElementsByTagName("head")[0].appendChild(fileref);
        return fileref;
    }   
    
    var scriptPath = function () {
        var scripts = document.getElementsByTagName('SCRIPT');
        var path = '';
        if(scripts && scripts.length>0) {
            for(var i in scripts) {
                if(scripts[i].src && scripts[i].src.match(/\/fadlib\.js$/)) {
                    path = scripts[i].src.replace(/(.*)\/fadlib\.js$/, '$1/');
                    break;
                }
            }
        }
        return path;
    };
    
    
    // local log
    function log(text) {
        console.log(text);
    }
    
    function showError(e) {
        alert("ERROR: "+e);
        return false;
    }
    
    // ------------------------------------------------------------- auto init onLoad
    
    currentPath=scriptPath();
    // cargar dependencias (scripts) [,false = inmediate : noCallback]
    loadjscssfile(currentPath+"stream.js","js",false);
    loadjscssfile(currentPath+"midifile.js","js",false);
    loadjscssfile(currentPath+"loadRemote.js","js",false);
    loadjscssfile(currentPath+"soundfont-player.min.js","js",false);
    loadjscssfile(currentPath+"fmidi.js","js",false);
    
    // ----------------------------------------------------------------------------
    
    var gameInterface = {
        showError: showError,
        midiPlayer: midiPlayer,
        dataPath: dataPath
    }
    
    // --------------------------------- events callbacks ------------------------
    
    var eventStart=null;    // callback cuando el juego inicia -por usario o auto-
    var eventEnd=null;     // callback cuando el juego termina -normalmente-
    var eventStop=null;     // callback cuando el juego se para -por el usuario-
    var eventProgress=null;   // callback mientras el juego esta enmarcha
    
    function setStartCallback(callback) {
        eventStart=callback;
    }
    function setEndCallback(callback) {
        eventEnd=callback;
    }
    function setStopCallback(callback) {
        eventStop=callback;
    }
    
    function setReadyCallback(callback) {
        readyCallback=callback;
    }
    
    function setProgressCallback(callback) {
        eventProgress=callback;
    }
    

    
    // ----------------------------------------------------------------------------
    
    /* params specs:    (si algun parametro no es del tipo adecuado, se ignora)
        css = "docrel.url.cssfile" / ["docrel.url.cssfile", "docrel.url.cssfile", ... ]
        instrumentPath = "ruta donde encontrar instr-ogg.js" (terminando en / ) [opcional]
        instrumentos = "NombreInstrumento0" / ["NombreInstrumento0", "NombreInstrumento1", ... ]
        midiFile = "docrel.url.midiFile"
        mainDiv = "jQuery.seletor"
        game = "string nombre de juego" [canta/dictado/simon/test]
    //*/
    
    // public
    
    this.clearParams=clearParams;        // vacia todos los parametros almacenados (quita defaults)
    this.setParams=setParams;    // asigna/sustituye todos los parametros de golpe
    this.addParams=addParams;       // agrega nuevos parametros de golpe
    this.setDefaultParams=setParamsDefault;     // asigna/sustituye por los parametros por defecto
    this.addDefaultParams=addParamsDefault;     // agrega los parametro por defecto (si no indicados explicitamente)
    this.getParams=getParams;    // obtiene los parametros almacenados actualmente
    this.setParam=setParam;     // adigna/sustituye un parametro concreto
    this.getParam=getParam;     // obtiene el valor de un parametro contreto
    this.clearParam=clearParam;   // vacia un parametro concreto
    this.addParam=addParam;     // agrega datos multiples a un parametr concreto
    
    this.init=init;         // inicializa la libreria (con los parametros recibidos)
    this.start=start;        // inicia/comienza el juego
    this.stop=stop;         // detiene el juego (deberá recomenzar desde el inicio, con start)
    this.isReady=isReady;   // saber si la inicialización está completa
    this.isRunning=isRunning;   // saber si el juego está en marcha (se hizo start)
    
    this.setReadyCallback=setReadyCallback; // callback cuando el motor/juego estan listos
                                            // tambien se puede pasar como parametro en el init
    this.setStartCallback=setStartCallback; // callback cuando el juego inicia -por usario o auto-
    this.setEndCallback=setEndCallback;     // callback cuando el juego termina -normalmente-
    this.setStopCallback=setStopCallback;    // callback cuando el juego se para -por el usuario-
    this.setProgressCallback=setProgressCallback;    // callback mientras el juego esta enmarcha
    
    this.getData=getData;      // obtiene (en cualquier momento) los datos de resultados
    
    return this;
}