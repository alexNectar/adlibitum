FAdLib_Game = function(_mainDiv, _params, _gameInterface) {
    console.log(":: FAdlib : Juego Tests ::");   
    
    var running=false;
    
    var mainDiv=_mainDiv;
    var params=_params;
    var gameInterface=_gameInterface;
    
    //parametros basicos (requeridos) : valor por defecto
    var paramsDefault={
        pentagrama: true,
        notas: true,
        velocidad: 100,
        previsualizar: 2000,
        soloTrack: null
    }
    for (var i in paramsDefault) {
        if (params[i]==undefined) params[i]=paramsDefault[i];
    }
    
    // -----------------------------------------------------------------------------
    
    var htmlLayout=`
    <div class="fadlib full vertical" id="fondo">
        <div class="fadlib horizontal full" id="cabecera">
            <div class="fadlib" id="controles">
                <img class="fadlib start" id="startstop" />
            </div>
            <div class="fadlib">
                <div class="fadlib" id="titulo">
                </div>
                <div class="fadlib" id="tiempo">tiempo</div>
            </div>
            <div class="fadlib" id="marcadores">
                <div class="fadlib right" id="marcador">
Aciertos: 0</br>
Fallos: 0</br>
Sin Responder: 0</div>
            </div>
        </div>
        <div class="fadlib test" id="test">
        </div>
    </div>
    `;
    
    
    var htmlPregunta=`
    <div class="fadlib pregunta">
        <div class="fadlib cabecera">
            <div class="fadlib titulo" id="titulo">Titulo</div>
            <div class="fadlib texto" id="texto">Texto</div>
            <img class="fadlib imagen" id="imagen"></img>
            <img class="fadlib midi" id="midi"></img>
        </div>
        <div class="fadlib respuestas" id="respuestas"></div>
    </div>
    `;
    
    var htmlRespuesta=`
        <div class="fadlib respuesta">
            <div class="fadlib id" id="id">A)</div>
            <div class="fadlib texto" id="texto">Texto</div>
            <img class="fadlib imagen" id="imagen"></img>
            <img class="fadlib midi" id="midi"></img>
            <input class="fadlib tick" id="tick" type="checkbox" name="0" value="0">
        </div>
    `;
    

    //      fadlib capa vcenter 
    // -----------------------------------------------------------------------------
    
    function isRunning() {
        return running;
    }
    
    var elJuego=null;
    var elTiempo=null;
    var elMarcador=null;
    var elStartStop=null;
    var tTiempo=null;
    
    function init() {
        // validar parametros (valores minimos)
        if (params.test.tiempo<1) params.test.tiempo=1;
        
        
        // crear elementos visuales
        mainDiv.append(htmlLayout);
        
        elJuego = $(".fadlib#test");
        elMarcador = $(".fadlib#marcador");
        elTiempo = $(".fadlib#tiempo");
        
        $(".fadlib#titulo").text(params.test.titulo);
        updateReloj(params.test.tiempo);
        
        elStartStop=$(".fadlib#startstop").click(function(e){
            if (running) stop(); else start();
        });
        
        updateMarcador();   
        
        elJuego.hide();
        prepararPreguntas();
        gameInterface.midiPlayer.setEndCallback(midiStop);
    }
    
    
    // ---------------------- game Init
    
    var respuestasIds=["A)","B)","C)","D)","E)","F)","G)","H)","I)","J)","K)","L)"];
    var elementos=[];
    var midiPlaying=null;
    function prepararPreguntas() {
        elementos=[];
        for (var p=0;p<params.preguntas.length;p++) {
            var elem={};
            var pregunta=params.preguntas[p];
            var elPregunta=$(htmlPregunta).appendTo(elJuego);
            elPregunta.find("#titulo").text(pregunta.titulo);
            elPregunta.find("#texto").text(pregunta.texto);
            if (pregunta.imagen!=undefined) {
                elPregunta.find("#imagen").attr("src",gameInterface.dataPath+pregunta.imagen);
            } else elPregunta.find("#imagen").hide();
            if (pregunta.midiFile!=undefined) {
                // cargar midi
                var midiButton=elPregunta.find("#midi");
                (function(midiFile){    // custom scope for var midiFile
                    midiButton.click(function(){
                        if (midiPlaying==this) { // si sonando yo: solo parar
                            midiStop(); 
                        } else {    // si sonando otro: parar el otro, y empezar yo
                            midiStop();
                            midiPlaying=this;
                            gameInterface.midiPlayer.prepare(midiFile, function(){
                                gameInterface.midiPlayer.play(playNote,params.velocidad,null,0);
                            });
                            $(this).addClass("playing");    
                        }
                    });
                })(gameInterface.dataPath+pregunta.midiFile);
            } else elPregunta.find("#midi").hide();
            
            elem.pregunta=elPregunta;
            elem.respuestas=[];
            
            for (var r=0;r<pregunta.respuestas.length;r++) {
                var respuesta=pregunta.respuestas[r];
                var elRespuesta=$(htmlRespuesta).appendTo(elPregunta);
                elRespuesta.find("#id").text(respuestasIds[r]);
                
                elRespuesta.find("input").attr("name",p);
                
                if (respuesta.imagen!=undefined) {
                    elRespuesta.find("#imagen").attr("src",gameInterface.dataPath+respuesta.imagen);
                } else elRespuesta.find("#imagen").hide();
                
                if (respuesta.midiFile!=undefined) {
                    // cargar midi
                    var midiButton=elRespuesta.find("#midi");
                    (function(midiFile){    // custom scope for var midiFile
                        midiButton.click(function(){
                            if (midiPlaying==this) { // si sonando yo: solo parar
                                midiStop(); 
                            } else {    // si sonando otro: parar el otro, y empezar yo
                                midiStop();
                                midiPlaying=this;
                                gameInterface.midiPlayer.prepare(midiFile, function(){
                                    gameInterface.midiPlayer.play(playNote,params.velocidad,null,0);
                                });
                                $(this).addClass("playing");    
                            }
                        });
                    })(gameInterface.dataPath+respuesta.midiFile);
                } else elRespuesta.find("#midi").hide();
                
				elRespuesta.find("#texto").text(respuesta.texto);
                if (respuesta.imagen!=undefined || respuesta.midiFile!=undefined) {
                    elRespuesta.find("#texto").hide();
                }
                
                if (respuesta.correcta) elRespuesta.attr("correcta","true");
                
                elem.respuestas.push(elRespuesta);
            }
            
            elementos.push(elem);
        }
        
        $("input.fadlib.tick").click(function(){
            if ($(this).is(":checked")) {
                // unchek los demas
                var _this=this;
                $(this).closest(".pregunta").find("input").each(
                    function(e){
                        if (this!=_this) $(this).attr("checked",false);
                    }
                );
            }
        });
    }
    
    function playNote(e) {
        gameInterface.midiPlayer.defaultEvent(e);
    }
    
    function midiStop() {
        if (midiPlaying) {
            
            $(midiPlaying).removeClass("playing");
            midiPlaying=null;
        }
        gameInterface.midiPlayer.stop();
    }
    
    // ---------------------- game logic
    
    var tiempoInicio=null;
    var tiempoJuego=0;
    var juegoCompletado=false;
    var aciertos=0;
    var fallos=0;
    var vacias=0;
    var puntos=0;
    
    function start() {
        if (running) return;
        
        tiempoInicio=new Date().getTime();  // miliseconds clock
        tiempoJuego=0;
        juegoCompletado=false;
        elStartStop.removeClass("start").addClass("stop");
        running=true;
        // informar de que el juego se ha iniciado -por usuario o auto-
        if (startCallback && typeof startCallback==="function") startCallback.call();
        
        elJuego.show();
        tTiempo=setInterval(tick, 1000);
    }
    
    
    function stop() {
        if (!running) return;
        
        // fin del tiempo
        clearInterval(tTiempo);
        juegoCompletado=true;
        running=false;
        elStartStop.removeClass("stop");//.addClass("start");
        
        elJuego.children().addClass("disabled");
        
        // corregir
        for (var p=0;p<elementos.length;p++) {
            var preg=elementos[p];
            
            var marcada=null;
            var correcta=null;
            for (var r=0;r<preg.respuestas.length;r++) {
                var res=preg.respuestas[r];
                if (res.find("input:checked").size()>0) {
                    marcada=res;
                    marcada.addClass("incorrecta");
                }
                if (res.attr("correcta")) {
                    correcta=res;
                    correcta.addClass("correcta").removeClass("incorrecta");
                }
            }
            
            if (marcada==null) {
                vacias++;
                puntos+=params.puntos.vacia;
            } else {
                if (marcada.get(0)==correcta.get(0)) {
                    aciertos++;
                    puntos+=params.puntos.acierto;
                    preg.pregunta.addClass("correcta");
                } else {
                    fallos++;
                    puntos+=params.puntos.fallo;
                    preg.pregunta.addClass("incorrecta");
                }
            }
        }
        
        updateMarcador();
        
        // informar de que el usuario lo ha detenido -manualmente-
        if (stopCallback && typeof stopCallback==="function") stopCallback.call();
    }
    
    function tick() {
        tiempoJuego++;
        var restante=params.test.tiempo-tiempoJuego;
        updateReloj(restante);
        if (restante<1) {
            // corregir automaticamente
            stop();
        }
    }
    
    function updateReloj(restante) {
         elTiempo.text(Math.floor(restante/60)+":"+pad(restante%60,2)+'"');
    }
    
    function pad(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }
    
    function updateMarcador() {
        // mostrar marcador
        var marcador=
            "Aciertos: "+aciertos+
            `</br>`+"Fallos: "+fallos+
            `</br>`+"Sin Responder: "+vacias+
            `</br></br>`+"Puntos: "+puntos;
        elMarcador.html(marcador);
        
        // informar del progreso
        if (progressCallback && typeof progressCallback==="function") progressCallback.call(null);
    }
    
    function end() {
        // midi finalizado
        // ---auto stop---
        //detenerJuego();
        //elStartStop.removeClass("stop").addClass("start");
        juegoCompletado=true;
        
        // mostrar respuestas correctas
        elJuego.find("[correcta]").addClass("correcta");
        
        // fin del juego : informar
        if (endCallback && typeof endCallback==="function") endCallback.call();
        
        tiempoInicio=null;
    }
    
    // ---------------------- game Data
    function getData() {
        if (tiempoInicio!=null) tiempoJuego=((new Date().getTime())-tiempoInicio)/1000; // tiempo en secs.
        
        return {
            aciertos: aciertos,
            errores: fallos,
            puntos: puntos,
            tiempo: tiempoJuego,
            completado: juegoCompletado
        }
    }
    
    // ------------------- set Events Callback ----------------------------------
    var startCallback=null;
    function setStartCallback(callback) {
        startCallback=callback;
    }
    var endCallback=null;
    function setEndCallback(callback) {
        endCallback=callback;
    }
    var stopCallback=null;
    function setStopCallback(callback) {
        stopCallback=callback;
    }
    
    var progressCallback=null;
    function setProgressCallback(callback) {
        progressCallback=callback;
    }
    
    
    // ------------------- public -----------------------------------------------
    
    this.init=init;
    this.start=start;
    this.stop=stop;
    this.isRunning=isRunning;
    this.getData=getData;
    this.setStartCallback=setStartCallback;
    this.setEndCallback=setEndCallback;
    this.setStopCallback=setStopCallback;
    this.setProgressCallback=setProgressCallback;

    return this;
}
    