$(function() {
    $("#browser iframe").hide();
    
    // acciones de mostrar u ocultar bloques según checkbox o number
    var list, list_num;
   
    // css
    list=$("#basicos #css");
    list_num=$("#basicos #css_num");
    itemsList(list, list_num.val());
    list_num.on("change",function(){
       itemsList($("#basicos #css"),$(this).val()); 
    });
    
    // instrumentos
    list=$("#basicos #instrumento");
    list_num=$("#basicos #instrumentos_num");
    itemsList(list, list_num.val());
    list_num.on("change",function(){
       itemsList($("#basicos #instrumento"),$(this).val()); 
    });
    
    // lanzadores
    list=$("#lanzadores #lanzador");
    list_num=$("#lanzadores #lanzador_num");
    itemsList(list, list_num.val());
    list_num.on("change",function(){
       itemsList($("#lanzadores #lanzador"),$(this).val()); 
    });

    
    // duplicar preguntas (por codigo)
    list_num=$("#juego_test #pregunta_num");
    var pregunta=$("#juego_test #preguntas > ul > li").detach();
    var contenedor=$("#juego_test #preguntas > ul");
    var maximas=list_num.attr("max");    
    // clonar
    for (var i=0;i<maximas;i++) {
        contenedor.append(pregunta.clone());
    }

    // mostrar/ocultar
    list=$("#juego_test #pregunta");
    itemsList(list, list_num.val());
    list_num.on("change",function() {
        itemsList(list, $(this).val());
    });

     // respuestas
    list=$("#juego_test #pregunta");
    list.each(function(index,element){
        //if (index>0) return;
        var resp_num=$(element).find("#respuesta_num");
        var resp=$(element).find("li #respuesta");
        itemsList(resp, resp_num.val());
        resp_num.on("change", function(){
            itemsList(resp, $(this).val());
        });
    });
    
    // metronomo
    $("input#metronomo").on("change", function() {
       if ($(this).get(0).checked) $(".block#metronomo").show();
        else $(".block#metronomo").hide();
    });
    $(".block#metronomo").hide();
    
    
    // tipo de juego
    $("#game").on("change",function(){
        // ocultar todos los bloques
        $("#juego_dictado").hide();
        $("#juego_simon").hide();
        $("#juego_test").hide();
        // mostrar según el seleccionado
        var seleccionado=$(this).val();
        if (seleccionado=="dictado") $("#juego_dictado").show();
        if (seleccionado=="simon") $("#juego_simon").show();
        if (seleccionado=="test") $("#juego_test").show();
        $("input[name=tipo]").val(seleccionado);
    });
    $("#game").change();
    
    
    populate();
    
    $(".file").on("click",fileFocus);
});

// --------------------------------- utils

function itemsList(items, amount) {
    if (!amount) amount=0;
    items.each(function(index,element){
        if (index<amount) $(element).parent().show();
        else $(element).parent().hide();
    });
}

function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}

var lastFocus=null;
function fileFocus() {
    lastFocus=$(this);
}
function fileSeleccionar(ruta) {
    if (lastFocus!=null) {
        lastFocus.val(ruta);
    }
}

// ------------------------

// botones formulario
function cancelar() {
    window.location="index.php";
}

function guardar() {
    // actualiza los campos
    // y envia el formulario a guardar
    if (update()) $("form#principal").submit();
}

// ------------------------

var juego;  // copia del "data" (update = form->juego  //  populate = juego->form)

function update() {
    // recoge de los apartados y campos detalle
    // y actualiza el textarea "data"
    juego={};
        // parametros generales
        // basicos
        juego.mainDiv=$("input#mainDiv").val();
        if ($("input#dataPath").val().length>0) juego.dataPath=$("input#dataPath").val();

        juego.css=[];
        var css_num=Number($("input#css_num").val());
        for (var i=0;i<css_num;i++) {
            if ($("input#css").eq(i).val().length>0) juego.css.push($("input#css").eq(i).val());
        }
        
        if ($("input#instrumentsPath").val().length>0) juego.instrumentsPath=$("input#instrumentsPath").val();
        juego.instrumentos=[];
        var instrumentos_num=Number($("input#instrumentos_num").val());
        for (var i=0;i<instrumentos_num;i++) {
            if ($("input#instrumento").eq(i).val().length>0) juego.instrumentos.push($("input#instrumento").eq(i).val());
        }

        
        // elementos a mostrar
        juego.pentagrama=$("input#pentagrama").get(0).checked;
        juego.notas=$("input#notas").get(0).checked;
        if ($("input#metronomo").get(0).checked) {
            juego.metronomo={};
            juego.metronomo.instrumento=Number($("#metronomo input#instrumento").val());
            juego.metronomo.nota=$("#metronomo input#nota").val();
            juego.metronomo.step=Number($("#metronomo input#step").val());
            juego.metronomo.prestep=Number($("#metronomo input#prestep").val());
        }
        if ($("input#compas").val().length>0) juego.compas=$("input#compas").val();
        
        // archivo midi
        juego.midiFile=$("input#midiFile").val();
        juego.velocidad=Number($("input#velocidad").val());
        if ($("input#soloTrack").val().length>0) juego.soloTrack=Number($("input#soloTrack").val());
        if ($("input#canalSilencios").val().length>0) juego.canalSilencios=Number($("input#canalSilencios").val());
        if ($("input#previsualizar").val().length>0) juego.previsualizar=Number($("input#previsualizar").val());
        
        // lanzadores
        var lanzador_num=Number($("input#lanzador_num").val());
        juego.lanzadores=[];
        for (var i=0;i<lanzador_num;i++) {
            var lanzador={};
            lanzador.class=$("#lanzadores #lanzador").eq(i).find("input#class").val();
            lanzador.nota=$("#lanzadores #lanzador").eq(i).find("input#nota").val();
            lanzador.instrumento=Number($("#lanzadores #lanzador").eq(i).find("input#instrumento").val());
            if ($("#lanzadores #lanzador").eq(i).find("input#tecla").val().length>0) lanzador.tecla=$("#lanzadores #lanzador").eq(i).find("input#tecla").val();
            juego.lanzadores.push(lanzador);
        }

        // error
        juego.error={};
        juego.error.nota=$("#error input#nota").val();
        juego.error.instrumento=Number($("#error input#instrumento").val());
        
        
        // datos adicionales
        var customData=$("textarea#customData").val();
        if (customData.length>0) {
            try {
                juego.customData=JSON.parse($("textarea#customData").val());
            } catch (e) {
                alert("customData no tiene formato JSON valido");
                return false;
            }
        }
        
        // juego
        juego.autoStart=$("input#autoStart").get(0).checked;
        juego.game=$("#game").val();
        
        // ----------------------------------------------------
        // --------------------- DICTADO ----------------------
        if (juego.game=="dictado") {
            // margenes
            juego.margenes={};
            juego.margenes.maximo=Number($("#juego_dictado #margenes input#maximo").val());
            juego.margenes.medio=Number($("#juego_dictado #margenes input#medio").val());

            // puntos
            juego.puntos={};
            juego.puntos.maximo=Number($("#juego_dictado #puntos input#maximo").val());
            juego.puntos.medio=Number($("#juego_dictado #puntos input#medio").val());
            juego.puntos.minimo=Number($("#juego_dictado #puntos input#minimo").val());
            juego.puntos.fallo=Number($("#juego_dictado #puntos input#fallo").val());
            juego.puntos.ignorada=Number($("#juego_dictado #puntos input#ignorada").val());
        }
        
        // --------------------- SIMON ----------------------
        if (juego.game=="simon") {
            // secuencias
            juego.secuencias={};
            juego.secuencias.tipo=$("#juego_simon #secuencias #tipo").val();
            juego.secuencias.inicio=Number($("#juego_simon #secuencias input#inicio").val());
            juego.secuencias.crecer=Number($("#juego_simon #secuencias input#crecer").val());
            juego.secuencias.ganar=Number($("#juego_simon #secuencias input#ganar").val());
            // puntos
            juego.puntos={};
            juego.puntos.fallo=Number($("#juego_simon #puntos input#fallo").val());
            juego.puntos.nota=$("#juego_simon #puntos input#nota").val();
            juego.puntos.secuencia=Number($("#juego_simon #puntos input#secuencia").val());
        }
        
        // --------------------- TEST ----------------------
        if (juego.game=="test") {
            // test
            juego.test={};
            juego.test.titulo=$("#juego_test #test input#titulo").val();
            juego.test.fallos=Number($("#juego_test #test input#fallos").val());
            juego.test.tiempo=Number($("#juego_test #test input#tiempo").val());
            // puntos
            juego.puntos={};
            juego.puntos.acierto=Number($("#juego_test #puntos input#acierto").val());
            juego.puntos.fallo=Number($("#juego_test #puntos input#fallo").val());
            juego.puntos.vacia=Number($("#juego_test #puntos input#vacia").val());
            // preguntas
            juego.preguntas=[];
            var pregunta_num=Number($("#juego_test #preguntas input#pregunta_num").val());
            for (var i=0;i<pregunta_num;i++) {
                var preguntaObj=$("#juego_test #preguntas li #pregunta").eq(i);
                var pregunta={};
                pregunta.titulo=preguntaObj.find("input#titulo").val();
                if (preguntaObj.find("input#texto").val().length>0) pregunta.texto=preguntaObj.find("input#texto").val();
                if (preguntaObj.find("input#imagen").val().length>0) pregunta.imagen=preguntaObj.find("input#imagen").val();
                if (preguntaObj.find("input#midiFile").val().length>0) pregunta.midiFile=preguntaObj.find("input#midiFile").val();

                pregunta.respuestas=[];
                var respuesta_num=Number(preguntaObj.find("#respuestas input#respuesta_num").val());
                for (var j=0;j<respuesta_num;j++) {
                    var respuestaObj=preguntaObj.find("#respuestas li #respuesta").eq(j);
                    var respuesta={};
                    if (respuestaObj.find("input#texto").val().length>0) respuesta.texto=respuestaObj.find("input#texto").val();
                    if (respuestaObj.find("input#imagen").val().length>0) respuesta.imagen=respuestaObj.find("input#imagen").val();
                    if (respuestaObj.find("input#midiFile").val().length>0) respuesta.midiFile=respuestaObj.find("input#midiFile").val();
                    respuesta.correcta=respuestaObj.find("input#correcta").get(0).checked;
                    pregunta.respuestas.push(respuesta);
                }
  
                juego.preguntas.push(pregunta);
            }
            
        }
    
    $("textarea[name=data]").val(JSON.stringify(juego));
    window.scrollTo(0,0);
    
    return true;
}





function populate() {
    // pasa del campo "data" (textarea)
    // a mostrar los apartados y campos detalle rellenados
    juego=undefined;
    try {
        juego=JSON.parse($("textarea[name=data]").val());
    } catch(e) {
        if (Number($("input[name=id]").val())>-1)
            alert("El juego estaba corrupto. Se muestran valors por defecto.");
    }
    if (juego!=undefined) {
        // parametros generales
        
        // basicos
        $("input#mainDiv").val(juego.mainDiv);
        if (juego.dataPath!=undefined) $("input#dataPath").val(juego.dataPath);
        if (isArray(juego.css)) {
            $("input#css_num").val(juego.css.length || 0);
            for (var i=0;i<juego.css.length;i++) {
                $("input#css").eq(i).val(juego.css[i]);
            }
        } else {
            if (juego.css!=undefined) {
                $("input#css_num").val(1);
                $("input#css").first().val(juego.css);
            }
        }
        $("input#css_num").change();
        
        if (juego.instrumentsPath!=undefined) $("input#instrumentsPath").val(juego.instrumentsPath);
        if (isArray(juego.instrumentos)) {
            $("input#instrumentos_num").val(juego.instrumentos.length || 0);
            for (var i=0;i<juego.instrumentos.length;i++) {
                $("input#instrumento").eq(i).val(juego.instrumentos[i]);
            }
        } else {
            if (juego.css!=undefined) {
                $("input#instrumentos_num").val(1);
                $("input#instrumento").first().val(juego.instrumentos);
            }
        }
        $("input#instrumentos_num").change();
        
        // elementos a mostrar
        $("input#pentagrama").get(0).checked=(juego.pentagrama);
        $("input#pentagrama").change();
        $("input#notas").get(0).checked=(juego.notas);
        $("input#notas").change();
        $("input#metronomo").get(0).checked=(juego.metronomo!=undefined);
        $("input#metronomo").change();
        if (juego.metronomo!=undefined) {
            $("#metronomo input#instrumento").val(juego.metronomo.instrumento);
            $("#metronomo input#nota").val(juego.metronomo.nota);
            $("#metronomo input#step").val(juego.metronomo.step);
            $("#metronomo input#prestep").val(juego.metronomo.prestep);
        }
        $("input#compas").val(juego.compas);
        
        // archivo midi
        $("input#midiFile").val(juego.midiFile);
        $("input#velocidad").val(juego.velocidad);
        $("input#soloTrack").val(juego.soloTrack);
        $("input#canalSilencios").val(juego.canalSilencios);
        $("input#previsualizar").val(juego.previsualizar);
        
        // lanzadores
        if (isArray(juego.lanzadores)) {
            $("input#lanzador_num").val(juego.lanzadores.length || 0);
            for (var i=0;i<juego.lanzadores.length;i++) {
                $("#lanzadores #lanzador").eq(i).find("input#class").val(juego.lanzadores[i].class);
                $("#lanzadores #lanzador").eq(i).find("input#nota").val(juego.lanzadores[i].nota);
                $("#lanzadores #lanzador").eq(i).find("input#instrumento").val(juego.lanzadores[i].instrumento);
                $("#lanzadores #lanzador").eq(i).find("input#tecla").val(juego.lanzadores[i].tecla);
            }
        }
        $("input#lanzador_num").change();
        
        // error
        if (juego.error!=undefined) {
            $("#error input#nota").val(juego.error.nota);
            $("#error input#instrumento").val(juego.error.instrumento);
        }
        
        // datos adicionales
        $("textarea#customData").val(JSON.stringify(juego.customData));
        
        // juego
        $("input#autoStart").get(0).checked=(juego.autoStart);
        $("input#autoStart").change();
        $("#game").val(juego.game);
        $("#game").change();
        
        
        // ----------------------------------------------------
        // --------------------- DICTADO ----------------------
        // margenes
        if (juego.margenes!=undefined) {
            $("#juego_dictado #margenes input#maximo").val(juego.margenes.maximo);
            $("#juego_dictado #margenes input#medio").val(juego.margenes.medio);
        }
        // puntos
        if (juego.puntos!=undefined) {
            $("#juego_dictado #puntos input#maximo").val(juego.puntos.maximo);
            $("#juego_dictado #puntos input#medio").val(juego.puntos.medio);
            $("#juego_dictado #puntos input#minimo").val(juego.puntos.minimo);
            $("#juego_dictado #puntos input#fallo").val(juego.puntos.fallo);
            $("#juego_dictado #puntos input#ignorada").val(juego.puntos.ignorada);
        }
        
        // --------------------- SIMON ----------------------
        // secuencias
        if (juego.secuencias!=undefined) {
            $("#juego_simon #secuencias #tipo").val(juego.secuencias.tipo);
            $("#juego_simon #secuencias input#inicio").val(juego.secuencias.inicio);
            $("#juego_simon #secuencias input#crecer").val(juego.secuencias.crecer);
            $("#juego_simon #secuencias input#ganar").val(juego.secuencias.ganar);
        }
        // puntos
        if (juego.puntos!=undefined) {
            $("#juego_simon #puntos input#fallo").val(juego.puntos.fallo);
            $("#juego_simon #puntos input#nota").val(juego.puntos.nota);
            $("#juego_simon #puntos input#secuencia").val(juego.puntos.secuencia);
        }
        
        // --------------------- TEST ----------------------
        // test
        if (juego.test!=undefined) {
            $("#juego_test #test input#titulo").val(juego.test.titulo);
            $("#juego_test #test input#fallos").val(juego.test.fallos);
            $("#juego_test #test input#tiempo").val(juego.test.tiempo);
        }
        // puntos
        if (juego.puntos!=undefined) {
            $("#juego_test #puntos input#acierto").val(juego.puntos.acierto);
            $("#juego_test #puntos input#fallo").val(juego.puntos.fallo);
            $("#juego_test #puntos input#vacia").val(juego.puntos.vacia);
        }
        // preguntas
        if (isArray(juego.preguntas)) {
            $("#juego_test #preguntas input#pregunta_num").val(juego.preguntas.length || 0);
            for (var i=0;i<juego.preguntas.length;i++) {
                var preguntaObj=$("#juego_test #preguntas li #pregunta").eq(i);
                var pregunta=juego.preguntas[i];
                preguntaObj.find("input#titulo").val(pregunta.titulo);
                preguntaObj.find("input#texto").val(pregunta.texto);
                preguntaObj.find("input#imagen").val(pregunta.imagen);
                preguntaObj.find("input#midiFile").val(pregunta.midiFile);
                
                preguntaObj.find("#respuestas input#respuesta_num").val(pregunta.respuestas.length || 0);
                for (var j=0;j<pregunta.respuestas.length;j++) {
                    var respuestaObj=preguntaObj.find("#respuestas li #respuesta").eq(j);
                    var respuesta=pregunta.respuestas[j];
                    respuestaObj.find("input#texto").val(respuesta.texto);
                    respuestaObj.find("input#imagen").val(respuesta.imagen);
                    respuestaObj.find("input#midiFile").val(respuesta.midiFile);
                    respuestaObj.find("input#correcta").get(0).checked=(respuesta.correcta);
                }
                preguntaObj.find("#respuestas input#respuesta_num").change();
            }
        }
        $("#juego_test #preguntas input#pregunta_num").change();
        
    }
}


// ------------------------

function probar() {
    try {
        // recojo todos los campos actuales (update)
        update();
        var form=$("form#probar");
        form.find("textarea[name=probarData]").val(JSON.stringify(juego));
        // abre una nueva ventana (flotante)
        window.open("","probar");
        form.submit();
        // a la que le envío por POST el data
    } catch(e) {
        alert("No es posible probar el juego.\nErrores en los datos del juego.")
    }
}


