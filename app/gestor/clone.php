<!DOCTYPE html>
<?php
require "connect.php";
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta lang="es-ES">
        <title>Adlibitum : Gestor Juegos - Clonar Juego</title>
    </head>
    <body>
        <h1>Clonar Juego</h1>
        <hr>

<?php
        if (isset($_GET["id"])) {
            $sql = "SELECT * FROM juegos WHERE id=".$_GET["id"];
            $res = mysqli_query($con, $sql);
            if ($res) {
                // encontrado
                $juego=mysqli_fetch_assoc($res);
                
                $sql = mysqli_prepare($con, "INSERT INTO juegos (nombre, tipo, nivel, data) VALUES (?,?,?,?)");
                mysqli_stmt_bind_param($sql,"ssis",$juego["nombre"],$juego["tipo"],$juego["nivel"],$juego["data"]);
                $res = mysqli_stmt_execute($sql);
                if ($res) {
                    // clonado
                    ?>
                    <p>El Juego ha sido clonado.</p>
                    <?php
                } else {
                    // error
                    ?>
                    <p>Error al clonar el Juego con id:<?= $_GET["id"] ?></p>
                    <div><?=mysqli_error($con) ?></div>
                    <?php
                }
            } else {
                // error
                ?>
                <p>Error. No se encuentra el Juego con id:<?= $_GET["id"] ?></p>
                <div><?=mysqli_error($con) ?></div>
                <?php
            }
        } else {
            // sin parametro
            ?>
                <p>Error: No se ha recibido el parametro requerido ID.</p>
            <?php
        }
?>
        <a href="index.php">Volver</a>
    </body>
</html>
<?php
mysqli_close($con);
?>