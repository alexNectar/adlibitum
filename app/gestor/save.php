<!DOCTYPE html>
<?php
require "connect.php";
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta lang="es-ES">
        <title>Adlibitum : Gestor Juegos - Guardar Juego</title>
    </head>
    <body>
        <h1>Guardar Juego</h1>
        <hr>
        <?php

        if (isset($_POST["id"]) && $_POST["id"]!="-1") {
            $sql = mysqli_prepare($con, "UPDATE juegos SET nombre=?, tipo=?, nivel=?, data=? WHERE id=".$_POST["id"]);
            mysqli_stmt_bind_param($sql,"ssis",$_POST["nombre"],$_POST["tipo"],$_POST["nivel"],$_POST["data"]);
            //$res = mysqli_query($con, $sql);
            $res = mysqli_stmt_execute($sql);
            if ($res) {
                // guardado
                ?>
                <p>El Juego Id: <?= $_POST["id"] ?> ha sido guardado.</p>
                <?php
            } else {
                // error
                ?>
                <p>Error al guardar el Juego con id:<?= $_POST["id"] ?></p>
                <div><?=mysqli_error($con) ?></div>
                <?php
            }
        } else {
            // sin parametro = juego nuevo
            $sql = mysqli_prepare($con, "INSERT INTO juegos (nombre, tipo, nivel, data) VALUES (?,?,?,?)");
            mysqli_stmt_bind_param($sql,"ssis",$_POST["nombre"],$_POST["tipo"],$_POST["nivel"],$_POST["data"]);
            $res = mysqli_stmt_execute($sql);
            //$res = mysqli_query($con, $sql);
            if ($res) {
                // creado
                ?>
                <p>El Juego ha sido creado.</p>
                <?php
            } else {
                // error
                ?>
                <p>Error al crear el Juego.</p>
                <div><?=mysqli_error($con) ?></div>
                <?php
            }
        }
        //*/
?>
        <a href="index.php">Volver</a>
    </body>
</html>
<?php
mysqli_close($con);
?>
