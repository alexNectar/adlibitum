<!DOCTYPE html>
<?php

require "config.php";
$dir = $local_path; //"files";
// Run the recursive function 
$tree = json_encode(scan($dir), JSON_UNESCAPED_SLASHES);

// This function scans the files folder recursively, and builds a large array
function scan($dir){

	$files = array();

	// Is there actually such a folder/file?

	if(file_exists($dir)){
	
		foreach(scandir($dir) as $f) {
		
			if(!$f || $f[0] == '.') {
				continue; // Ignore hidden files
			}

			if(is_dir($dir . '/' . $f)) {

				// The path is a folder

				$files[] = array(
					"name" => $f,
					"type" => "folder",
					"path" => $dir . '/' . $f,
					"items" => scan($dir . '/' . $f) // Recursively get the contents of the folder
				);
			}
			
			else {

				// It is a file

				$files[] = array(
					"name" => $f,
					"type" => "file",
					"path" => $dir . '/' . $f,
					"size" => filesize($dir . '/' . $f) // Gets the size of this file
				);
			}
		}
	
	}

	return $files;
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta lang="es-ES">
        <title>Adlibitum : Gestor Juegos - Seleccionar Archivo</title>
    </head>
    <body style="background-color: white;">
        <div onclick="window.location.reload();"><small>(Actualizar lista de archivos)</small></div>
        <div id="nav"></div>
        <div id="files">
            <ul>
                <li>Item1</li>
            </ul>
        </div>
    </body>
    <script src="jquery-1.11.3.min.js"></script>
    <script>
        var tree=null;
        var prefix=null;
        var nav=[];
        var current=null;
        $(function() {
          tree=JSON.parse('<?=$tree?>');
          prefix='<?=$dir?>';
          nav=[]; navList=[];
          current=tree;
            
          show();
        });

        function show() {
            var ruta="Ruta: "+prefix.slice(0,-1);
            for (var i=0;i<navList.length;i++) ruta=ruta+"/"+navList[i];
            $("#nav").text(ruta);
            
            var files=$("#files");
            files.find("li").remove();
            if (nav.length>0) {
                var fdiv=$("<li>[..]</li>").appendTo(files).on("click",goparent);
            }
            for (var i=0;i<current.length;i++) {
                var f=current[i];
                var fdiv=$("<li></li>").text(f.name).appendTo(files);
                fdiv.data(f);
                if (f.type=="file") {
                    fdiv.on("click",seleccionar).addClass("file");
                } else if (f.type=="folder") {
                    fdiv.on("click",entrar).addClass("folder");
                }
            }
        }

        function entrar() {
            var fdiv=$(this);
            var f=fdiv.data();
            nav.push(current);
            navList.push(f.name);
            current=f.items;
            show();
        }

        function seleccionar() {
            var fdiv=$(this);
            var f=fdiv.data();
            var path=f.path.replace(prefix+"/","");
            if (window.parent!=undefined && window.parent.fileSeleccionar!=undefined)
                window.parent.fileSeleccionar(path);
        }
    
        function goparent() {
            var f=nav.pop();
            current=f;
            navList.pop();
            show();
        }
    </script>
    <style>
        .folder:before {
            content: "[";
        }
        .folder:after {
            content: "]";
        }
    </style>
</html>
