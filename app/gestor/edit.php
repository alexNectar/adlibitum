<!DOCTYPE html>
<?php
require "config.php";
require "connect.php";

$nuevo=true;
$juego=null;

?>

<html>
    <head>
        <meta charset="utf-8">
        <meta lang="es-ES">
        <title>Adlibitum : Gestor Juegos - Editar Juego</title>
        <link rel="stylesheet" href="edit.css">
    </head>
    <body>
        <div id="browser">
            <div  onclick='$("#browser iframe").toggle();'>Seleccionar archivos</div>
            <iframe src="browser.php"></iframe>
        </div>
        <h1>Editar/Crear Juego</h1>
        <hr>
        <?php
        if (isset($_GET["id"])) {
            $sql = "SELECT * FROM juegos WHERE id=".$_GET["id"];
            $res = mysqli_query($con, $sql);
            if ($res) {
                // encontrado
                $juego=mysqli_fetch_assoc($res);
                $nuevo=false;
            } else {
                // error
                ?>
                <p>Error. No se encuentra el Juego con id:<?= $_GET["id"] ?></p>
                <div><?=mysqli_error($con) ?></div>
                <a href="index.php">Volver</a>
                <?php
            }
        } else {
            // sin parametro = crear nuevo
            $juego=[];
            $juego["id"]=-1;
            $juego["nombre"]="";
            $juego["tipo"]="dictado";
            $juego["nivel"]=1;
            $juego["data"]="";
            $nuevo=true;
        }
?>
<?php if ($juego!=null || $nuevo) { ?>
        <form method="post" action="probar.php" id="probar" target="probar">
            <textarea name="probarData" style="display:none;"></textarea>
        </form>
        <form method="post" action="save.php" id="principal">
            <div class="block">
                <div>
                    <div class="caption">Id:</div>
                    <input type="number" name="id" value="<?= ($nuevo)?"-1":$juego["id"] ?>" readonly="true" class="field">
                    <div class="tip">i<span>El id es asignado de forma automática.</span></div>
                </div>
                <div>
                    <div class="caption">Nombre:</div>
                    <input type="text" name="nombre" value="<?=$juego["nombre"] ?>" class="field">
                    <div class="tip">i<span>Nombre para identificar el juego. (no se mostrará al jugar)</span></div>
                </div>
                <div>
                    <div class="caption">Tipo:</div>
                    <input type="text" name="tipo" readonly="true" value="<?=$juego["tipo"] ?>" readonly="true" class="field">
                    <div class="tip">i<span>Tipo de juego. Seleccionar en el apartado "Juego" (ver mas abajo)</span></div>
                </div>
                <div>
                    <div class="caption">Nivel:</div>
                    <input type="number" name="nivel" value="<?=$juego["nivel"] ?>" class="field">
                    <div class="tip">i<span>Nivel al que corresponde el juego. (para clasificar, no se mostrará al jugar)</span></div>
                </div>
                <div>
                    <div class="caption">Data:</div>
                    <textarea name="data" readonly="true" class="field"><?=$juego["data"]?></textarea>
                    <div class="tip">i<span>Datos adicionales. No modifican el comportamiento del juego. (ver mas abajo)</span></div>
                </div>
            </div>
            <div class="block">
                <button id="Cancelar" onclick="cancelar();" type="button">Cancelar</button>
                <button id="Guardar" onclick="guardar();" type="button">Guardar</button>
                &nbsp; &nbsp; &nbsp;
                <button id="Probar" onclick="probar();" type="button">Probar</button>
            </div>
        </form>
        <hr>
        
        <h1>Parametros Generales:</h1>
        
        
        <div id="basicos" class="block">
            <h2>Basicos:</h2>
            <div>
                <div class="caption">mainDiv:</div>
                <input type="text" id="mainDiv" class="field" placeholder="#mainDiv" value="#mainDiv">
                <div class="tip">i<span>Area contenedor del juego en la pagina web. (selector jQuery)</span></div>
            </div>
            <div>
                <div class="caption">dataPath:</div>
                <input type="text" id="dataPath" class="field" value="<?=$web_path?>">
                <div class="tip">i<span>Prefijo de ruta para acceder a los archivos externos. (midi, jpgs, css, etc)</span></div>
            </div>
            <div>
                <div class="caption">css:</div>
                <input type="number" id="css_num" class="field" min=0 max=10 value=0>
                <div class="tip">i<span>Lista de archivos CSS a cargar.</span></div>
            </div>
            <ul class="list">
                <li><input type="text" id="css" class="field file"></li>
                <li><input type="text" id="css" class="field file"></li>
                <li><input type="text" id="css" class="field file"></li>
                <li><input type="text" id="css" class="field file"></li>
                <li><input type="text" id="css" class="field file"></li>
                <li><input type="text" id="css" class="field file"></li>
                <li><input type="text" id="css" class="field file"></li>
                <li><input type="text" id="css" class="field file"></li>
                <li><input type="text" id="css" class="field file"></li>
                <li><input type="text" id="css" class="field file"></li>
            </ul>
            <div>
                <div class="caption">instrumentsPath:</div>
                <input type="text" id="instrumentsPath" class="field" value="<?=$instruments_path?>">
                <div class="tip">i<span>Prefijo de ruta para acceder a los instrumentos.</span></div>
            </div>
            <div>
                <div class="caption">instrumentos:</div>
                <input type="number" id="instrumentos_num" class="field" min=0 max=10 value=0>
                <div class="tip">i<span>Lista de instrumentos a cargar. [nombres]</span></div>
            </div>
            <ul class="list">
                <li>0 <input type="text" id="instrumento" class="field"></li>
                <li>1 <input type="text" id="instrumento" class="field"></li>
                <li>2 <input type="text" id="instrumento" class="field"></li>
                <li>3 <input type="text" id="instrumento" class="field"></li>
                <li>4 <input type="text" id="instrumento" class="field"></li>
                <li>5 <input type="text" id="instrumento" class="field"></li>
                <li>6 <input type="text" id="instrumento" class="field"></li>
                <li>7 <input type="text" id="instrumento" class="field"></li>
                <li>8 <input type="text" id="instrumento" class="field"></li>
                <li>9 <input type="text" id="instrumento" class="field"></li>
            </ul>
        </div>
        
        
        <div class="block">
            <h2>Elementos a mostrar:</h2>
            <div>
                <div class="caption">pentagrama:</div>
                <input type="checkbox" id="pentagrama" class="field">
                <div class="tip">i<span>Se mostrarán las líneas de un pentagrama.</span></div>
            </div>
            <div>
                <div class="caption">notas:</div>
                <input type="checkbox" id="notas" class="field">
                <div class="tip">i<span>Se mostrarán las notas (en su posición y con su figura correspondiente)</span></div>
            </div>
            <div>
                <div class="caption">Metronomo:</div>
                <input type="checkbox" id="metronomo">
                <div class="tip">i<span>Usar metrónomo</span></div>
            </div>
            <div id="metronomo" class="block">
                <div>
                    <div class="caption">instrumento:</div>
                    <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                    <div class="tip">i<span>Instrumento que se utilizará para dar sonido al metrónomo. [nombre]</span></div>
                </div>
                <div>
                    <div class="caption">nota:</div>
                    <input type="text" id="nota" class="field">
                    <div class="tip">i<span>Nota con la que sonará el metrónomo. [ej: a5]</span></div>
                </div>
                <div>
                    <div class="caption">step:</div>
                    <input type="number" id="step" class="field" min=1 max=10 value=2>
                    <div class="tip">i<span>Cada cuantos tiempos sonará el metrónomo. [1=todos]</span></div>
                </div>
                <div>
                    <div class="caption">prestep:</div>
                    <input type="number" id="prestep" class="field" min=0 max=10 value=2>
                    <div class="tip">i<span>Cuantos ticks sonarán antes de comenzar la partitura.</span></div>
                </div>
            </div>
            <div>
                <div class="caption">compas:</div>
                <input type="text" id="compas" class="field" placeholder="4/4">
                <div class="tip">i<span>Indica cada cuanto tiempo se dibujará línea de fin de comás. [ej: 2/4]</span></div>
            </div>
        </div>    
        
        
        <div class="block">
            <h2>Archivo Midi:</h2>
            <div>
                <div class="caption">midiFile:</div>
                <input type="text" id="midiFile" class="field file">
                <div class="tip">i<span>Archivo principal para el juego [ignorado en juego:Test]</span></div>
            </div>
            <div>
                <div class="caption">velocidad:</div>
                <input type="number" id="velocidad" class="field" min=1 max=200 value=100>
                <div class="tip">i<span>Velocidad de reproducción el Midi [100=normal, 200=doble]</span></div>
            </div>
            <div>
                <div class="caption">soloTrack:</div>
                <input type="number" id="soloTrack" class="field" min=0 max=127 value=1>
                <div class="tip">i<span>Número de canal para mostrar notas. [el resto suenan, pero no se muestran]</span></div>
            </div>
            <div>
                <div class="caption">canalSilencios:</div>
                <input type="number" id="canalSilencios" class="field" min=0 max=127 value=0>
                <div class="tip">i<span>Número de canal cuyas notas serán dibujadas como silencios</span></div>
            </div>
            <div>
                <div class="caption">previsualizar:</div>
                <input type="number" id="previsualizar" class="field" min=0 max=10000 value=3000 step=100>
                <div class="tip">i<span>Tiempo (en milisegundos) desde que aparece la nota, hasta llegar a sonar.</span></div>
            </div>
        </div>
        
        
        <div id="lanzadores" class="block">
            <h2>Lanzadores:</h2>
            <div class="caption">lanzadores:</div>
            <input type="number" id="lanzador_num" class="field" min=0 max=10 value=0>
            <div class="tip">i<span>Pulsadores para tocar las notas visualmente.</span></div>
            <ul class="list">
                <li>
                    <div id="lanzador" class="block">
                        <div>
                            <div class="caption">class:</div>
                            <input type="text" id="class" class="field">
                            <div class="tip">i<span>Clase de estilo a aplicar a este lanzador.</span></div>
                        </div>
                        <div>
                            <div class="caption">nota:</div>
                            <input type="text" id="nota" class="field" placeholder="c4">
                            <div class="tip">i<span>Nota a sonar con este lanzador. [ej: c4]</span></div>
                        </div>
                        <div>
                            <div class="caption">instrumento:</div>
                            <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                            <div class="tip">i<span>Instrumento con el que sonará este lanzador</span></div>
                        </div>
                        <div>
                            <div class="caption">tecla:</div>
                            <input type="text" id="tecla" class="field" maxlength="1">
                            <div class="tip">i<span>Letra del teclado que servirá para pulsar este lanzador.</span></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="lanzador" class="block">
                        <div>
                            <div class="caption">class:</div>
                            <input type="text" id="class" class="field">
                            <div class="tip">i<span>Clase de estilo a aplicar a este lanzador.</span></div>
                        </div>
                        <div>
                            <div class="caption">nota:</div>
                            <input type="text" id="nota" class="field" placeholder="c4">
                            <div class="tip">i<span>Nota a sonar con este lanzador. [ej: c4]</span></div>
                        </div>
                        <div>
                            <div class="caption">instrumento:</div>
                            <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                            <div class="tip">i<span>Instrumento con el que sonará este lanzador</span></div>
                        </div>
                        <div>
                            <div class="caption">tecla:</div>
                            <input type="text" id="tecla" class="field" maxlength="1">
                            <div class="tip">i<span>Letra del teclado que servirá para pulsar este lanzador.</span></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="lanzador" class="block">
                        <div>
                            <div class="caption">class:</div>
                            <input type="text" id="class" class="field">
                            <div class="tip">i<span>Clase de estilo a aplicar a este lanzador.</span></div>
                        </div>
                        <div>
                            <div class="caption">nota:</div>
                            <input type="text" id="nota" class="field" placeholder="c4">
                            <div class="tip">i<span>Nota a sonar con este lanzador. [ej: c4]</span></div>
                        </div>
                        <div>
                            <div class="caption">instrumento:</div>
                            <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                            <div class="tip">i<span>Instrumento con el que sonará este lanzador</span></div>
                        </div>
                        <div>
                            <div class="caption">tecla:</div>
                            <input type="text" id="tecla" class="field" maxlength="1">
                            <div class="tip">i<span>Letra del teclado que servirá para pulsar este lanzador.</span></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="lanzador" class="block">
                        <div>
                            <div class="caption">class:</div>
                            <input type="text" id="class" class="field">
                            <div class="tip">i<span>Clase de estilo a aplicar a este lanzador.</span></div>
                        </div>
                        <div>
                            <div class="caption">nota:</div>
                            <input type="text" id="nota" class="field" placeholder="c4">
                            <div class="tip">i<span>Nota a sonar con este lanzador. [ej: c4]</span></div>
                        </div>
                        <div>
                            <div class="caption">instrumento:</div>
                            <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                            <div class="tip">i<span>Instrumento con el que sonará este lanzador</span></div>
                        </div>
                        <div>
                            <div class="caption">tecla:</div>
                            <input type="text" id="tecla" class="field" maxlength="1">
                            <div class="tip">i<span>Letra del teclado que servirá para pulsar este lanzador.</span></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="lanzador" class="block">
                        <div>
                            <div class="caption">class:</div>
                            <input type="text" id="class" class="field">
                            <div class="tip">i<span>Clase de estilo a aplicar a este lanzador.</span></div>
                        </div>
                        <div>
                            <div class="caption">nota:</div>
                            <input type="text" id="nota" class="field" placeholder="c4">
                            <div class="tip">i<span>Nota a sonar con este lanzador. [ej: c4]</span></div>
                        </div>
                        <div>
                            <div class="caption">instrumento:</div>
                            <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                            <div class="tip">i<span>Instrumento con el que sonará este lanzador</span></div>
                        </div>
                        <div>
                            <div class="caption">tecla:</div>
                            <input type="text" id="tecla" class="field" maxlength="1">
                            <div class="tip">i<span>Letra del teclado que servirá para pulsar este lanzador.</span></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="lanzador" class="block">
                        <div>
                            <div class="caption">class:</div>
                            <input type="text" id="class" class="field">
                            <div class="tip">i<span>Clase de estilo a aplicar a este lanzador.</span></div>
                        </div>
                        <div>
                            <div class="caption">nota:</div>
                            <input type="text" id="nota" class="field" placeholder="c4">
                            <div class="tip">i<span>Nota a sonar con este lanzador. [ej: c4]</span></div>
                        </div>
                        <div>
                            <div class="caption">instrumento:</div>
                            <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                            <div class="tip">i<span>Instrumento con el que sonará este lanzador</span></div>
                        </div>
                        <div>
                            <div class="caption">tecla:</div>
                            <input type="text" id="tecla" class="field" maxlength="1">
                            <div class="tip">i<span>Letra del teclado que servirá para pulsar este lanzador.</span></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="lanzador" class="block">
                        <div>
                            <div class="caption">class:</div>
                            <input type="text" id="class" class="field">
                            <div class="tip">i<span>Clase de estilo a aplicar a este lanzador.</span></div>
                        </div>
                        <div>
                            <div class="caption">nota:</div>
                            <input type="text" id="nota" class="field" placeholder="c4">
                            <div class="tip">i<span>Nota a sonar con este lanzador. [ej: c4]</span></div>
                        </div>
                        <div>
                            <div class="caption">instrumento:</div>
                            <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                            <div class="tip">i<span>Instrumento con el que sonará este lanzador</span></div>
                        </div>
                        <div>
                            <div class="caption">tecla:</div>
                            <input type="text" id="tecla" class="field" maxlength="1">
                            <div class="tip">i<span>Letra del teclado que servirá para pulsar este lanzador.</span></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="lanzador" class="block">
                        <div>
                            <div class="caption">class:</div>
                            <input type="text" id="class" class="field">
                            <div class="tip">i<span>Clase de estilo a aplicar a este lanzador.</span></div>
                        </div>
                        <div>
                            <div class="caption">nota:</div>
                            <input type="text" id="nota" class="field" placeholder="c4">
                            <div class="tip">i<span>Nota a sonar con este lanzador. [ej: c4]</span></div>
                        </div>
                        <div>
                            <div class="caption">instrumento:</div>
                            <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                            <div class="tip">i<span>Instrumento con el que sonará este lanzador</span></div>
                        </div>
                        <div>
                            <div class="caption">tecla:</div>
                            <input type="text" id="tecla" class="field" maxlength="1">
                            <div class="tip">i<span>Letra del teclado que servirá para pulsar este lanzador.</span></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="lanzador" class="block">
                        <div>
                            <div class="caption">class:</div>
                            <input type="text" id="class" class="field">
                            <div class="tip">i<span>Clase de estilo a aplicar a este lanzador.</span></div>
                        </div>
                        <div>
                            <div class="caption">nota:</div>
                            <input type="text" id="nota" class="field" placeholder="c4">
                            <div class="tip">i<span>Nota a sonar con este lanzador. [ej: c4]</span></div>
                        </div>
                        <div>
                            <div class="caption">instrumento:</div>
                            <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                            <div class="tip">i<span>Instrumento con el que sonará este lanzador</span></div>
                        </div>
                        <div>
                            <div class="caption">tecla:</div>
                            <input type="text" id="tecla" class="field" maxlength="1">
                            <div class="tip">i<span>Letra del teclado que servirá para pulsar este lanzador.</span></div>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="lanzador" class="block">
                        <div>
                            <div class="caption">class:</div>
                            <input type="text" id="class" class="field">
                            <div class="tip">i<span>Clase de estilo a aplicar a este lanzador.</span></div>
                        </div>
                        <div>
                            <div class="caption">nota:</div>
                            <input type="text" id="nota" class="field" placeholder="c4">
                            <div class="tip">i<span>Nota a sonar con este lanzador. [ej: c4]</span></div>
                        </div>
                        <div>
                            <div class="caption">instrumento:</div>
                            <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                            <div class="tip">i<span>Instrumento con el que sonará este lanzador</span></div>
                        </div>
                        <div>
                            <div class="caption">tecla:</div>
                            <input type="text" id="tecla" class="field" maxlength="1">
                            <div class="tip">i<span>Letra del teclado que servirá para pulsar este lanzador.</span></div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        
        
        <div id="error" class="block">
            <h2>Error:</h2>
            <div>
                <div class="caption">nota:</div>
                <input type="text" id="nota" class="field" placeholder="a3">
                <div class="tip">i<span>Nota a sonar cuando se cometa un error. [ej: a3]</span></div>
            </div>
            <div>
                <div class="caption">instrumento:</div>
                <input type="number" id="instrumento" class="field" min=0 max=127 value=0>
                <div class="tip">i<span>Instrumento a utilizar cuando se cometa un error</span></div>
            </div>
        </div>
        
        <div class="block">
            <h2>Datos adicionales:</h2>
            <div class="caption">Datos adicionales</div>
            <textarea id="customData" class="field"></textarea>
            <div class="tip">i<span>Datos de libre uso. (cadena en formato JSON)</span></div>
        </div>

        
        <div class="block">
            <h2>Juego:</h2>
            <div>
                <div class="caption">game:</div>
                <select type="text" id="game" class="field">
                    <option value="canta" selected>Canta conmigo</option>
                    <option value="dictado">Dictado</option>
                    <option value="simon">Simón</option>
                    <option value="test">Test de Preguntas</option>
                </select>
                <div class="tip">i<span>Tipo de Juego.</span></div>
            </div>
            <div>
                <div class="caption">autoStart:</div>
                <input type="checkbox" id="autoStart" class="field" checked=true>
                <div class="tip">i<span>Iniciar el Juego automáticamente al ser cargado.</span></div>
            </div>
        </div>

        
        
        
        <hr>
        
        <h1>Detalles del Juego:</h1>
        
        
        
        
        
        <div id="juego_dictado" class="block">
            <h1>Dictado:</h1>
            <div id="margenes" class="block">
                <h2>Margenes:</h2>
                <div>
                    <div class="caption">maximo:</div>
                    <input type="number" id="maximo" class="field" min=0 max=100 value=75>
                    <div class="tip">i<span>Distancia (en %) antes de la cual se otorga puntuación máxima.</span></div>
                </div>
                <div>
                    <div class="caption">medio:</div>
                    <input type="number" id="medio" class="field" min=0 max=100 value=50>
                    <div class="tip">i<span>Distancia (en %) antes de la cual se otorga puntuación media. (después: mínima)</span></div>
                </div>
            </div>
            <div id="puntos" class="block">
                <h2>Puntos:</h2>
                <div>
                    <div class="caption">maximo:</div>
                    <input type="number" id="maximo" class="field" min=-100 max=100 value=100>
                    <div class="tip">i<span>Puntuación al acertar dentro del límite máximo.</span></div>
                </div>
                <div>
                    <div class="caption">medio:</div>
                    <input type="number" id="medio" class="field" min=-100 max=100 value=50>
                    <div class="tip">i<span>Puntuación al acertar dentro del límite medio.</span></div>
                </div>
                <div>
                    <div class="caption">minimo:</div>
                    <input type="number" id="minimo" class="field" min=-100 max=100 value=25>
                    <div class="tip">i<span>Puntuación al acerta antes de llegar al límite medio</span></div>
                </div>
                <div>
                    <div class="caption">fallo:</div>
                    <input type="number" id="fallo" class="field" min=-100 max=100 value=-50>
                    <div class="tip">i<span>Puntuación (negativa?) al fallar una nota.</span></div>
                </div>
                <div>
                    <div class="caption">ignorada:</div>
                    <input type="number" id="ignorada" class="field" min=-100 max=100 value=0>
                    <div class="tip">i<span>Puntuación (ninguna?) al dejar pasar una nota.</span></div>
                </div>
            </div>
        </div>
        
        
        
        <div id="juego_simon" class="block">
            <h1>Simon:</h1>
            <div id="secuencias" class="block">
                <h2>secuencias:</h2>
                <div>
                    <div class="caption">tipo:</div>
                    <select id="tipo" class="field">
                        <option value="random">Random</option>
                        <option value="ranom-sec">Random-sec</option>
                        <option value="midi">Midi</option>
                        <option value="midi-sec">Midi-sec</option>
                    </select>
                    <div class="tip">i<span>
                        Tipo de secuencias a generar.<br>
                        Random: aleatorias sin conservar notas de la secuencia anterior<br>
                        Random-sec: aleatorias, agregando nuevas notas a la secuencia inicial<br>
                        Midi: tomando fragmentos del midi. (siempre manteniendo notas iniciales)<br>
                        Midi-sec: tomando fragmentos del midi. (avanzando sin repetir notas anteriores)
                    </span></div>
                </div>
                <div>
                    <div class="caption">inicio:</div>
                    <input type="number" id="inicio" class="field" min=-100 max=100 value=3>
                    <div class="tip">i<span>Cantidad de notas en la primera secuencia.</span></div>
                </div>
                <div>
                    <div class="caption">crecer:</div>
                    <input type="number" id="crecer" class="field" min=-100 max=100 value=2>
                    <div class="tip">i<span>Cantidad de notas a agregar en cada siguiente secuencia.</span></div>
                </div>
                <div>
                    <div class="caption">ganar:</div>
                    <input type="number" id="ganar" class="field" min=-100 max=100 value=3>
                    <div class="tip">i<span>Cantidad de notas a alcanzar para dar por ganado el juego.</span></div>
                </div>
            </div>
            <div id="puntos" class="block">
                <h2>Puntos:</h2>
                <div>
                    <div class="caption">fallo:</div>
                    <input type="number" id="fallo" class="field" min=-100 max=100 value=-50>
                    <div class="tip">i<span>Puntos (negativos?) al fallar la secuencia.</span></div>
                </div>
                <div>
                    <div class="caption">nota:</div>
                    <input type="number" id="nota" class="field" min=-100 max=100 value=10>
                    <div class="tip">i<span>Puntos al acertar una nota.</span></div>
                </div>
                <div>
                    <div class="caption">secuencia:</div>
                    <input type="number" id="secuencia" class="field" min=-100 max=100 value=100>
                    <div class="tip">i<span>Puntos (adicionales) al acertar una secuencia completa.</span></div>
                </div>
            </div>
        </div>

        
        
        <div id="juego_test" class="block">
            <h1>Juego Test:</h1>
            <div id="test" class="block">
                <h2>Test:</h2>
                <div>
                    <div class="caption">titulo:</div>
                    <input type="text" id="titulo" class="field">
                    <div class="tip">i<span>Titulo del Test. (aparecerá en la cabecera)</span></div>
                </div>
                <div>
                    <div class="caption">fallos:</div>
                    <input type="number" id="fallos" class="field" min=0 max=100 value=5>
                    <div class="tip">i<span>Cantidad de fallos permitidos para aprobar el test.</span></div>
                </div>
                <div>
                    <div class="caption">tiempo:</div>
                    <input type="number" id="tiempo" class="field" min=0 max=3600>
                    <div class="tip">i<span>Tiempo (en segundos) para realizar el Test.</span></div>
                </div>
            </div>
            <div id="puntos" class="block">
                <h2>Puntos:</h2>
                <div>
                    <div class="caption">acierto:</div>
                    <input type="number" id="acierto" class="field" min=0 max=100 value=100>
                    <div class="tip">i<span>Puntos por pregunta acertada.</span></div>
                </div>
                <div>
                    <div class="caption">fallo:</div>
                    <input type="number" id="fallo" class="field" min=-100 max=100 value=-50>
                    <div class="tip">i<span>Puntos (negativos?) por pregunta incorrecta.</span></div>
                </div>
                <div>
                    <div class="caption">vacia:</div>
                    <input type="number" id="vacia" class="field" min=-100 max=100 value=0>
                    <div class="tip">i<span>Puntos (ninguno?) por pregunta no contestada.</span></div>
                </div>
            </div>
            <div id="preguntas" class="block">
                <h2>Preguntas:</h2>
                <div class="caption">preguntas:</div>
                <input type="number" id="pregunta_num" class="field" min=0 max=20 value=0>
                <div class="tip">i<span>Preguntas del Test.</span></div>
                <ul class="list">
                    <li>
                        <div id="pregunta" class="block">
                            <div>
                                <div class="caption">titulo:</div>
                                <input type="text" id="titulo" class="field">
                                <div class="tip">i<span>Encabezado (breve) de la pregunta.</span></div>
                            </div>
                            <div>
                                <div class="caption">texto:</div>
                                <input type="text" id="texto" class="field">
                                <div class="tip">i<span>Descripción de la pregunta.</span></div>
                            </div>
                            <div>
                                <div class="caption">imagen:</div>
                                <input type="text" id="imagen" class="field file">
                                <div class="tip">i<span>Se emplea una imagen como pregunta. (se ignora el texto)</span></div>
                            </div>
                            <div>
                                <div class="caption">midiFile:</div>
                                <input type="text" id="midiFile" class="field file">
                                <div class="tip">i<span>Se emplea un Midi como pregunta. (se ignoran texto e imagen)</span></div>
                            </div>
                            <div id="respuestas">
                                <div class="caption">respuestas:</div>
                                <input type="number" id="respuesta_num" class="field" min=0 max=5 value=0>
                                <div class="tip">i<span>Respuestas a esta pregunta.</span></div>
                                <ul class="list">
                                    <li>
                                        <div id="respuesta" class="block">
                                            <div>Respuesta 1:</div>
                                            <div>
                                                <div class="caption">texto:</div>
                                                <input type="text" id="texto" class="field">
                                                <div class="tip">i<span>Texto de la respuesta.</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">imagen:</div>
                                                <input type="text" id="imagen" class="field file">
                                                <div class="tip">i<span>Imagen como respuesta (se ignora el texto).</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">midiFile:</div>
                                                <input type="text" id="midiFile" class="field file">
                                                <div class="tip">i<span>sonido Midi como respuesta (se ignoran texto e imagen).</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">correcta:</div>
                                                <input type="checkbox" id="correcta" class="field">
                                                <div class="tip">i<span>Considerar esta respuesta como correcta.</span></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div id="respuesta" class="block">
                                            <div>Respuesta 2:</div>
                                            <div>
                                                <div class="caption">texto:</div>
                                                <input type="text" id="texto" class="field">
                                                <div class="tip">i<span>Texto de la respuesta.</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">imagen:</div>
                                                <input type="text" id="imagen" class="field file">
                                                <div class="tip">i<span>Imagen como respuesta (se ignora el texto).</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">midiFile:</div>
                                                <input type="text" id="midiFile" class="field file">
                                                <div class="tip">i<span>sonido Midi como respuesta (se ignoran texto e imagen).</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">correcta:</div>
                                                <input type="checkbox" id="correcta" class="field">
                                                <div class="tip">i<span>Considerar esta respuesta como correcta.</span></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div id="respuesta" class="block">
                                            <div>Respuesta 3:</div>
                                            <div>
                                                <div class="caption">texto:</div>
                                                <input type="text" id="texto" class="field">
                                                <div class="tip">i<span>Texto de la respuesta.</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">imagen:</div>
                                                <input type="text" id="imagen" class="field file">
                                                <div class="tip">i<span>Imagen como respuesta (se ignora el texto).</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">midiFile:</div>
                                                <input type="text" id="midiFile" class="field file">
                                                <div class="tip">i<span>sonido Midi como respuesta (se ignoran texto e imagen).</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">correcta:</div>
                                                <input type="checkbox" id="correcta" class="field">
                                                <div class="tip">i<span>Considerar esta respuesta como correcta.</span></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div id="respuesta" class="block">
                                            <div>Respuesta 4:</div>
                                            <div>
                                                <div class="caption">texto:</div>
                                                <input type="text" id="texto" class="field">
                                                <div class="tip">i<span>Texto de la respuesta.</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">imagen:</div>
                                                <input type="text" id="imagen" class="field file">
                                                <div class="tip">i<span>Imagen como respuesta (se ignora el texto).</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">midiFile:</div>
                                                <input type="text" id="midiFile" class="field file">
                                                <div class="tip">i<span>sonido Midi como respuesta (se ignoran texto e imagen).</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">correcta:</div>
                                                <input type="checkbox" id="correcta" class="field">
                                                <div class="tip">i<span>Considerar esta respuesta como correcta.</span></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div id="respuesta" class="block">
                                            <div>Respuesta 5:</div>
                                            <div>
                                                <div class="caption">texto:</div>
                                                <input type="text" id="texto" class="field">
                                                <div class="tip">i<span>Texto de la respuesta.</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">imagen:</div>
                                                <input type="text" id="imagen" class="field file">
                                                <div class="tip">i<span>Imagen como respuesta (se ignora el texto).</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">midiFile:</div>
                                                <input type="text" id="midiFile" class="field file">
                                                <div class="tip">i<span>sonido Midi como respuesta (se ignoran texto e imagen).</span></div>
                                            </div>
                                            <div>
                                                <div class="caption">correcta:</div>
                                                <input type="checkbox" id="correcta" class="field">
                                                <div class="tip">i<span>Considerar esta respuesta como correcta.</span></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>    
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        
        
        <hr>
        <button id="Update" onclick="update();">Update Data</button>

<?php   } ?>
    </body>
    <script src="jquery-1.11.3.min.js"></script>
    <script src="edit.js"></script>
</html>
<?php
mysqli_close($con);
?>
