<!DOCTYPE html>
<?php
require "config.php";
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta lang="es-ES">
        <title>Adlibitum : Gestor Juegos - Probar Juego</title>
        <script src="<?=$lib_path?>fadlib/jquery-1.11.3.min.js"></script>
        <script src="<?=$lib_path?>fadlib/fadlib.js"></script>
    </head>
    <body>
        <h1>Probar Juego</h1>
        <div id="mainDiv" style="width:885px; height:530px;"></div>
        <hr>
        <?php
        if (isset($_POST["probarData"])) {
                ?>
                <button id="setParams">setParams</button>
                <button id="init">Init</button>
                    <br/>
                <button id="start">Start</button>
                <button id="stop">Stop</button>
                    <br/><br/>
                <p>Parametros del Juego:</p>
                <textarea id="params" style="display: inline-block; height:200px; width:55%" readonly="true"><?= $_POST["probarData"] ?></textarea>
                <textarea id="progress" style="display: inline-block; height:200px; width:40%"></textarea>
        
                <script>
            var fadlib=null;
            $(function() {
                $("#start, #stop, #init").hide();
                $("#setParams").click(function(){
                    var params=$("#params").val();
                    fadlib.setParams(params); 
                    $("#start, #stop").hide();
                    $("#init").show();
                });
                $("#init").click(function(){
                   fadlib.init(); 
                });
                $("#start").click(function(){
                   fadlib.start(); 
                });
                $("#stop").click(function(){
                   fadlib.stop(); 
                });
                
                fadlib = new FAdLib();

                fadlib.setStartCallback(function() {
                    console.log("_________ GAME START ________"); 
                });
                fadlib.setEndCallback(function(data) {
                    console.log("_________ GAME END ________"); 
                    mostrarData(data);
                });
                fadlib.setStopCallback(function(data) {
                    console.log("_________ GAME STOP ________"); 
                    mostrarData(data);
                });
                fadlib.setReadyCallback(function(){
                    $("#start, #stop").show(); 
                });
                fadlib.setProgressCallback(mostrarData);
                
                
                function mostrarData(data){
                    var t="customData:\n";
                    for (var i in data.customData) {
                        t+="   "+i+" = "+data.customData[i]+"\n";
                    }
                    t+="\ngameData:\n"
                    for (var i in data.gameData) {
                        t+="   "+i+" = "+data.gameData[i]+"\n";
                    }
                   $("#progress").text(t); 
                }
                
                // poner los parametros (e iniciar) automaticamente
                $("#setParams").click();
                $("#init").click();
            });

        </script>
        
                <?php
        } else {
                ?>
                <p>No se han recibido los Parametros del Juego.</p>
                <?php
        }
?>
    </body>
</html>
