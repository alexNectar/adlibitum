<!DOCTYPE html>
<?php
require "connect.php";

$sql = "SELECT * FROM juegos";

$order="id";
$dir="ASC";
if (isset($_GET["order"])) {
    if ($_GET["order"]=="id") $order="id";
    if ($_GET["order"]=="nombre") $order="nombre";
    if ($_GET["order"]=="nivel") $order="nivel";
    if ($_GET["order"]=="tipo") $order="tipo";
    if (isset($_GET["dir"]) && $_GET["dir"]=="DESC") $dir="DESC";
}

$sql=$sql." ORDER BY ".$order." ".$dir;
$res = mysqli_query($con, $sql);

?>

<html>
    <head>
        <meta charset="utf-8">
        <meta lang="es-ES">
        <title>Adlibitum : Gestor Juegos</title>
    </head>
    <body>
        <h1>Listado de Juegos</h1>
        <hr>
        <p><a href="edit.php">Crear Juego Nuevo</a></p>
        <table>
            <thead>
                <tr>
                    <th><a href="?order=id<?=($order=="id" && $dir=="ASC")?"&dir=DESC":""?>">Id</a></th>
                    <th><a href="?order=nombre<?=($order=="nombre" && $dir=="ASC")?"&dir=DESC":""?>">Nombre</a></th>
                    <th><a href="?order=tipo<?=($order=="tipo" && $dir=="ASC")?"&dir=DESC":""?>">Tipo</a></th>
                    <th><a href="?order=nivel<?=($order=="nivel" && $dir=="ASC")?"&dir=DESC":""?>">Nivel</a></th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
<?php
if (mysqli_num_rows($res)>0) {
    while($row=mysqli_fetch_assoc($res)) {
?>
                <tr>
                    <td><?= $row["id"]; ?></td>
                    <td><?= $row["nombre"]; ?></td>
                    <td><?= $row["tipo"]; ?></td>
                    <td><?= $row["nivel"]; ?></td>
                    <td><a href="clone.php?id=<?=$row["id"]?>">clonar</a>
                        : <a href="edit.php?id=<?=$row["id"]?>">editar</a>
                        : <a href="#" onclick="confirmarEliminar(<?=$row["id"]?>,'<?=$row["nombre"]?>');">eliminar</a></td>
                </tr>
<?php
    }
} else {
    ?>
    <tr>
        <td colspan="5">No existen juegos a mostrar.</td>
    </tr>
    <?php
}
?>
            </tbody>
        </table>
        <hr>
    </body>
    <script>
        function confirmarEliminar(id,nombre) {
            if (confirm("Confirme la eliminación del Juego:\nId "+id+" : "+nombre)) {
                window.location="delete.php?id="+id;
            }
        }
    </script>
</html>


<?php
mysqli_close($con);
?>


<div class="fadlib capa full" id="notas">
    <div class="fadlib capa compas resaltar" style="left: 4.64002%;"></div>
    <div class="fadlib capa nota negra resaltar" style="top: 62.5%; left: 4.64002%;"><img></div>
    <div class="fadlib capa silencio corchea" style="left: 52.4%;"><img></div>
    <div class="fadlib capa nota negra" style="top: 50%; left: 57.62%;"><img></div>
    <div class="fadlib capa silencio semicorchea" style="left: 67.74%;"><img></div>
    <div class="fadlib capa nota negra" style="top: 37.5%; left: 70.66%;"><img></div>
</div>
