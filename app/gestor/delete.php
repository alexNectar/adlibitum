<!DOCTYPE html>
<?php
require "connect.php";
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta lang="es-ES">
        <title>Adlibitum : Gestor Juegos - Eliminar Juego</title>
    </head>
    <body>
        <h1>Eliminar Juego</h1>
        <hr>
        <?php
        if (isset($_GET["id"])) {
            $sql = "DELETE FROM juegos WHERE id=".$_GET["id"];
            $res = mysqli_query($con, $sql);
            if ($res) {
                // borrado
                ?>
                <p>El Juego Id: <?= $_GET["id"] ?> ha sido eliminado.</p>
                <?php
            } else {
                // error
                ?>
                <p>Error eliminando el Juego con id:<?= $_GET["id"] ?></p>
                <div><?=mysqli_error($con) ?></div>
                <?php
            }
        } else {
            // sin parametro
            ?>
                <p>Error: No se ha recibido el parametro requerido ID.</p>
            <?php
        }
?>
        <a href="index.php">Volver</a>
    </body>
</html>
<?php
mysqli_close($con);
?>
